def get_bits_set(mask, n_words, word_size):
    bits_set = []
    for j in range(0,n_words):
        word = mask[j]
        for i in range(0,word_size):
            bit = (word & pow(2,i)) / pow(2,i)
            if bit != 0:
                bits_set.append(j*word_size + i)
    return bits_set

def get_mask(bits, n_words, word_size):
    trigger_mask = n_words* [0x0]
    for bit in bits:
        word = bit / word_size
        trigger_mask[word] = trigger_mask[word] | pow(2,bit % word_size )
    trigger_mask = [ hex(mask) for mask in trigger_mask]
    return trigger_mask


word_size = 32
n_words = 4

trigger_masks = {
    "All" : [0xffffffff,0xffffffff,0xffffffff,0xffffffff],
    "Tracker" : [0x0,0x00200000,0x0,0x0],
    "Rich" : [0x0,0x00400000,0x0,0x0],
    "Velo" : [0x0,0x00000008,0x0,0x0],
    "Muon" : [0x0,0x00000400,0x0,0x0],
    "Calo" : [0x0,0x18000,0x0,0x0],
    "VClose" : [0x0,0x100,0x0,0x0],
    "Tell1Error" : [0x0,0x2000000,0x0,0x0]
}

for name, trigger_mask in trigger_masks.iteritems():
    print name, " : ", get_bits_set(trigger_mask,n_words,word_size)


print get_mask([47,48],n_words,word_size)
