#!/usr/bin/env lb-run 
# args: --use=TurboStreamProd Moore/latest python
# Run with ./checkTurboStreamProd.py --tck=0x215517a7 --year=2017 [--turcal]
import argparse
import importlib
from TCKUtils.utils import *

# options
parser = argparse.ArgumentParser(description="")
#parser.add_argument("--turbo", action='store_true', help="Turbo streams")
parser.add_argument("--turcal", action='store_true', help="Check TurCal streams")
parser.add_argument("--year", help="Stream definition")
parser.add_argument("--tck", help="TCK")
options = parser.parse_args()

tck = int(options.tck,0)

hlt2_lines = getHlt2Lines(int(options.tck,0))

streams_module = importlib.import_module("TurboStreamProd.streams_{}".format(options.year))
if options.turcal:
    streams = streams_module.turcal_streams
    turbo_lines = [line for line in hlt2_lines if line[-6:]=="TurCal" ]
else:
    streams = streams_module.turbo_streams
    turbo_lines = [line for line in hlt2_lines if line[-5:]=="Turbo" ]

lines_in_streams = []
for streams in streams.values():
    lines_in_streams += streams["lines"]
difference = set(turbo_lines).difference(set(lines_in_streams))
if len(difference)>0:
    print "Please add the following lines to TurboStreamProd"
    print difference
