from operator import itemgetter
from itertools import product
import os, sys, re, atexit
from collections import defaultdict
from HltPiquetScripts.SFTPClient import SFTPClient
import shelve

total = 0

db = shelve.open('files.db')
if 'files' in db:
    nodes = db['nodes']
    files = db['files']
    for v in files.itervalues():
        for e in v:
            total += e[1]
else:
    files = defaultdict(set)
    nodes = set()

def write_files():
    db['nodes'] = nodes
    db['files'] = files
    db.close()

import atexit
atexit.register(write_files)

source_dir = '/localdisk/Alignment/Calo'


#runnrs = [174819, 174822, 174823, 174824, 175266, 175269, 175359, 175363, 175364, 175385, 175431, 175434, 175492, 175497, 175499, 175580, 175584, 175585, 175589]


#Mup (fill 5352)
#runnrs = range(184205,184310)

#Mdown (fill 5439)
#runnrs = range(185312,185337)

#Both
runnrs = range(184205,184310)
runnrs += range(185312,185337)



re_file = re.compile(r"(?:Run_)?(0*(%s)).*\.(mdf|raw)" % ('|'.join((str(r) for r in runnrs))))




re_file = re.compile(r"(?:Run_)?(0*(%s)).*\.(mdf|raw)" % ('|'.join((str(r) for r in runnrs))))

for node in product(['a', 'e'], range(1, 11), range(1, 29)):
# for node in ['storerecv%02d' % d for d in range(1, 5)]:
    hostname = 'hlt%s%02d%02d' % node
    if hostname in nodes:
        print hostname, 'already done'
        continue
    with SFTPClient('cofitzpa', hostname) as sftp:
        if not sftp:
            continue
        try:
            contents = sftp.listdir(source_dir)
        except IOError as e:
            print hostname, e
            continue
        if not contents:
            continue
        for f in contents:
            m = re_file.match(os.path.split(f)[-1])
            if m:
                r = int(m.group(1))
                f = os.path.join(source_dir, f)
                s = sftp.stat(f).st_size
                total += s
                filelist = ("PFN:ssh://%s:%s" % (hostname, f.encode('ascii')))
                print filelist
                files[r].add(("PFN:ssh://%s:%s" % (hostname, f.encode('ascii')), s))

    print hostname, (float(total) / 1024 ** 3)
    nodes.add(hostname)
    if total > 250 * 1024 ** 3:
        break
