from time import sleep
import os, sys, re, select
from random import shuffle
from multiprocessing import Process, Queue
import socket, sys
from itertools import product
from collections import defaultdict
import zmq

from HltPiquetScripts.Parallel import Worker, Runner
from HltPiquetScripts.SFTPClient import SFTPCopier

class Copier(Runner):
    def __init__(self, processes, runs, dest_dir, max_size = 10 * 1024, max_run = 10):
        self.__runs = runs
        self.__max_size = float(max_size)
        self.__files = defaultdict(set)
        self.__copied_size = defaultdict(int)
        self.__re = re.compile(r"Run_0*([0-9]+)_.*\.(mdf|raw)")
        self.__dest_dir = dest_dir
        self.__n_stop = 0

        # Reply to requests what to do:
        # - "stop": do not retrieve file and stop now
        # - "last": retrieve file and stop if successful
        # - "no" : do not retrieve file
        # - "yes" :  retrieve file
        self.__rep = Runner.context.socket(zmq.REP)
        self.__rep.bind("ipc://%s" % os.path.join(self.tmpdir(), "copy_data"))

        super(Copier, self).__init__(processes, {self.__rep : {zmq.POLLIN : self.__handle_rep}},
                                     max_run)

        if not os.path.exists(dest_dir):
            os.makedirs(dest_dir)

        for f in os.listdir(dest_dir):
            st = os.stat(os.path.join(dest_dir, f))
            self.__add_file(f, float(st.st_size) / (1024 ** 2))
        print 'Already present files: %d MiB' % sum(self.__copied_size.itervalues())
        for info in self.__copied_size.iteritems():
            print 'For run %d: %f MiB' % info

    def __handle_rep(self):
        r, f, s = self.__rep.recv_pyobj()
        # print 'handle rep: run %d, copied size %f, max size %f' % (r, self.__copied_size[r], self.__max_size / len(self.__runs))
        if self.__copied_size[r] > (self.__max_size / len(self.__runs)):
            rep = 'no'
        elif (self.__copied_size[r] + s) > (self.__max_size / len(self.__runs)):
            rep = 'last'
        elif r in self.__files and f in self.__files[r]:
            try:
                st = os.stat(os.path.join(self.__dest_dir, f))
                if ((float(st.st_size) / (1024 ** 2)) - s) < 1e5:
                    rep = 'no'
                else:
                    rep = 'yes'
            except IOError:
                rep = 'yes'
        elif self.stop(""):
            rep = 'stop'
        else:
            rep = 'yes'
        self.__rep.send(rep)

    def stop(self, message):
        # Some printout
        if (self.__n_stop % 100) == 99:
            for info in self.__copied_size.iteritems():
                print 'For run %d: %f MiB' % info
        self.__n_stop += 1

        if message:
            f, s = message
            self.__add_file(f, s)
        total = sum(self.__copied_size.itervalues())
        return total > self.__max_size and all([v > (self.__max_size / float(len(self.__runs))) for v in self.__copied_size.itervalues()])

    def __add_file(self, f, s):
        m = self.__re.match(os.path.split(f)[-1])
        if m:
            r = int(m.group(1))
            self.__files[r].add(f)
            self.__copied_size[r] += s

    def files(self):
        files = {}
        for r, fs in self.__files.items():
            files[r] = sorted(list(fs))
        return files

def get_runs(min_run):
    run_db = shelve.open('run.db')
    runs = run_db['runs']
    return sorted([r for r in runs.iterkeys() if r > min_run])

nodes = ['hlt%s%02d%02d' % p for p in product('abcdef', range(1, 11), range(1, 33))]
shuffle(nodes)

# runs = [167020, 167023, 167024, 167025, 167026, 167028]
source_dir = '/localdisk/L0Emulation'
runs = get_runs(nodes[0], 162421, source_dir.strip('/'))
dest_dir = '/localdisk/hlt1/raaij/0x1606'
copiers = [SFTPCopier(i, 'raaij', node, runs, source_dir, dest_dir) for i, node in enumerate(nodes)]

# Test if there is a key
import paramiko
agent = paramiko.Agent()
agent_keys = agent.get_keys()
if len(agent_keys) == 0:
    print 'No ssh-agent, or no keys in it.'
    sys.exit(-1)

# Copy the files from the nodes
copier = Copier(copiers, runs, max_size = 18 * 1024, dest_dir = dest_dir, max_run = 10)
copier.run_processes()
