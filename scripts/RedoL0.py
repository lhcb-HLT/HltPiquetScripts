import os, sys, re
from collections import defaultdict
from HltPiquetScripts.Parallel import Worker, Runner

class Redoer(Worker):
    def __init__(self, n, input_files, output_file, tck):
        super(Redoer, self).__init__(n)
        self.__input_files = input_files
        self.__output_file = output_file
        self.__written = 0
        self.__tck = tck

    def run(self):
        # General configuration
        from Gaudi.Configuration import ApplicationMgr
        from Configurables import L0App
        app = L0App()
        app.ReplaceL0Banks = True
        app.outputFile = ""
        app.EvtMax = -1
        app.TCK = self.__tck
        app.DataType = '2015'

        app.CondDBtag = 'cond-20150828'
        app.DDDBtag = 'dddb-20150724'

        from GaudiConf import IOHelper
        IOHelper("MDF", "MDF").inputFiles(self.__input_files)

        from Configurables import EventSelector
        EventSelector().PrintFreq = 1000

        # Updated L0 decoding and filter
        from Configurables import L0DUFromRawAlg
        l0du_alg = L0DUFromRawAlg()
        from Configurables import LoKiSvc
        LoKiSvc().Welcome = False
        from Configurables import LoKi__L0Filter
        filter_code = "L0_DECISION_PHYSICS"
        l0_filter = LoKi__L0Filter('L0Filter', Code = filter_code)

        # Plug routing bits filter in before the L0 emulation sequence
        from Configurables import HltRoutingBitsFilter
        rb_filter = HltRoutingBitsFilter( "RoutingBitsFilter" )
        rb_filter.RequireMask = [0x0, 0x4, 0x0]

        # HLT Bank killer
        from Configurables import bankKiller
        hlt_banks = [ 'HltDecReports','HltRoutingBits','HltSelReports','HltVertexReports','HltLumiSummary','HltTrackReports' ]
        killer = bankKiller( 'RemoveInputHltRawBanks',  BankTypes = hlt_banks )

        # ODIN filter
        from Configurables import LoKi__ODINFilter as ODINFilter
        from Configurables import createODIN
        odinFilter = ODINFilter("ODINPhysicsFilter", Code = "ODIN_TRGTYP == LHCb.ODIN.PhysicsTrigger")

        ApplicationMgr().TopAlg = [createODIN(), killer]

        # MDF writer
        from Configurables import GaudiSequencer
        seq = GaudiSequencer('WriterSeq')

        filename = 'RedoL0_' + app.TCK
        from Configurables import LHCb__MDFWriter as MDFWriter
        mdf_writer = MDFWriter('MDFWriter', Compress = 0, ChecksumType = 1, GenerateMD5 = True,
                               Connection = 'file://%s' % self.__output_file)

        # Sequence is an OutStream so it runs after the L0 emulation stuff
        # seq.Members = [l0du_alg, l0_filter, mdf_writer]
        seq.Members = [l0du_alg, mdf_writer]
        ApplicationMgr().OutStream = [seq]

        # Timing table to make sure things work as intended
        from Configurables import AuditorSvc, LHCbTimingAuditor
        ApplicationMgr().AuditAlgorithms = 1
        if 'AuditorSvc' not in ApplicationMgr().ExtSvc:
            ApplicationMgr().ExtSvc.append('AuditorSvc')
        AuditorSvc().Auditors.append(LHCbTimingAuditor(Enable = True))

        from GaudiPython import AppMgr
        gaudi = AppMgr()
        gaudi.initialize()
        gaudi.run(app.EvtMax)
        gaudi.stop()
        gaudi.finalize()

        self.done()

# source_dir = '/localdisk/hlt1/raaij/nobias'
# files = defaultdict(set)
# re_file = re.compile(r"2015NB_25ns_0x00A2_([0-9]+)_.*\.(mdf|raw)")
# for f in os.listdir(source_dir):
#     m = re_file.match(os.path.split(f)[-1])
#     if m:
#         r = int(m.group(1))
#         files[r].add(f)

# dest_dir = '/localdisk/hlt1/raaij/redo'
tck = '0x00A8'
# of_pat = os.path.join(dest_dir, '2015NB_25ns_{0}_%d_%02d.mdf'.format(tck))
# files = {r : sorted([os.path.join(source_dir, f) for f in v]) for r, v in files.iteritems()}
# Redoers = []
# for i, (r, input_file) in enumerate([(r, input_file) for r, input_files in files.iteritems() for input_file in input_files]):
#     Redoers += [Redoer(i, [input_file], of_pat % (r, i), tck)]

# files = {'full'   : ['full_164699_0000000090.raw'],
#          'turbo'  : ['turbo_164699_0000000088.raw'],
#          'turcal' : ['turcal_164699_0000000087.raw']}


# source_dir = '/scratch/hlt/make_tcks'
# dest_dir = '/localdisk/hlt1/raaij/make_tcks'

# redoers = []
# for i, (stream, fs) in enumerate(files.items()):
#     of = os.path.join(dest_dir, stream + '_0x00A8.mdf')
#     redoers += [Redoer(i, [os.path.join(source_dir, f) for f in fs], of, tck)]

redoers = [Redoer(0, ['MakeTCK_physics_HLT2.mdf'], 'MakeTCK_physics_HLT2_%s.mdf' % tck, tck)]

runner = Runner(redoers)
runner.run_processes()
