import os, sys, re
from collections import defaultdict
from HltPiquetScripts.Parallel import Worker, Runner

class Cleaner(Worker):
    def __init__(self, n, input_files, output_file, split_by_mag = False):
        super(Cleaner, self).__init__(n)
        self.__process = Process(target = self.run)
        self.__input_files = input_files
        self.__output_file = output_file
        self.__written = 0
        self.__split_by_mag = split_by_mag
        
        run_db = shelve.open('run.db')
        self.__runs = run_db['runs']
        
    def run(self):
        from Configurables import LHCbApp
        app = LHCbApp()
        app.EvtMax = -1
        app.DataType = '2015'
        app.CondDBtag = 'cond-20150724'
        app.DDDBtag = 'dddb-20150828'
        # app.Simulation = True
  
        from Configurables import CondDB
        CondDB().Online = True

        from Configurables import EventSelector
        EventSelector().PrintFreq = 1000
 
        from GaudiConf import IOHelper
        IOHelper("MDF", "MDF").inputFiles(self.__input_files)
 
        # ODIN
        from Configurables import createODIN
        
        # Bank killer
        from Configurables import bankKiller
        hlt_banks = [ 'HltDecReports','HltRoutingBits','HltSelReports','HltVertexReports','HltLumiSummary','HltTrackReports' ]
        killer = bankKiller( 'RemoveInputHltRawBanks',  BankTypes = hlt_banks )
 
        # MDF writer
        from Configurables import LHCb__MDFWriter as MDFWriter
        if self.__split_by_mag:
            writers = {k : MDFWriter('MDFWriter%s' % v, Compress = 0, ChecksumType = 1, GenerateMD5 = True,
                                     Connection = 'file://' + self.__output_file.format(v, self.index())) for k, v in {-1 : 'Down', 1 : 'Up'}.iteritems()}
        else:
            writers = {'All' : MDFWriter('MDFWriter', Compress = 0, ChecksumType = 1, GenerateMD5 = True,
                                         Connection = 'file://' + self.__output_file.format(self.index()))}

        # Sequence
        from Configurables import GaudiSequencer
        seq = GaudiSequencer("Sequence", Members = [createODIN(), killer] + writers.values())
        from Gaudi.Configuration import ApplicationMgr
        ApplicationMgr().TopAlg = [seq]
 
        # Timing table to make sure things work as intended
        from Configurables import AuditorSvc, LHCbTimingAuditor
        ApplicationMgr().AuditAlgorithms = 1
        if 'AuditorSvc' not in ApplicationMgr().ExtSvc:
            ApplicationMgr().ExtSvc.append('AuditorSvc')
        AuditorSvc().Auditors.append(LHCbTimingAuditor(Enable = True))
 
        from GaudiPython import AppMgr
        gaudi = AppMgr()
        gaudi.initialize()
        TES = gaudi.evtSvc()

        writer_algs = {k : gaudi.algorithm(v.getName()) for k, v in writers.iteritems()}
        for writer in writer_algs.itervalues():
            writer.Enable = False

        n = 0
        while (app.EvtMax == -1) or n < app.EvtMax:
            gaudi.run(1)
            if not TES['/Event']:
                break
            n += 1

            run = TES['DAQ/ODIN'].runNumber()
            if run not in self.__runs:
                continue

            if self.__split_by_mag:
                writer = writer_algs[self.__runs[run]]
            else:
                writer = writer_algs['All']
            writer.Enable = True
            writer._ialg.execute()
            writer.Enable = False
            self.__written += 1

        gaudi.stop()
        gaudi.finalize()
        gaudi.exit()
        self.publish('written', self.__written)
        self.done()
 
source_dir = '/localdisk/hlt1/nobias'
files = defaultdict(set)
re_file = re.compile(r"Run_0*([0-9]+)_.*\.(mdf|raw)")
for f in os.listdir(source_dir):
    m = re_file.match(os.path.split(f)[-1])
    if m:
        r = int(m.group(1))
        files[r].add(f)

dest_dir = '/localdisk/hlt1/cleaned'
of_pat = os.path.join(dest_dir, 'Run_%06d_%02d.mdf')
files = {r : sorted([os.path.join(source_dir, f) for f in v]) for r, v in files.iteritems()}
Cleaners = []
for i, (r, input_file) in enumerate([(r, input_file) for r, input_files in files.iteritems() for input_file in input_files]):
    Cleaners += [Cleaner(i, [input_file], of_pat % (r, i), split_by_mag = False)]

runner = Runner(Cleaners)
runner.run_processes()
