import os, sys, re, shelve
from collections import defaultdict
from HltPiquetScripts.Parallel import Worker, Runner

class Redoer(Worker):
    def __init__(self, n, input_files, output_file, tck):
        super(Redoer, self).__init__(n)
        self.__input_files = input_files
        self.__output_file = output_file
        self.__written = 0
        self.__tck = tck

    def input_files(self):
        return self.__input_files
        
    def run(self):
        # General configuration
        from Gaudi.Configuration import ApplicationMgr
        from Configurables import L0App
        app = L0App()
        app.ReplaceL0Banks = True
        app.outputFile = ""
        app.EvtMax = -1
        app.TCK = self.__tck
        app.DataType = '2016'
        app.ReplayL0DUOnly = True
        app.WriteFSR = False

        app.CondDBtag = 'cond-20160522'
        app.DDDBtag = 'dddb-20150724'

        from FileStager.Configuration import configureFileStager
        configureFileStager()
        
        from GaudiConf import IOHelper
        IOHelper("MDF", "MDF").inputFiles(self.__input_files)

        from Configurables import CondDB
        conddb = CondDB()
        conddb.EnableRunStampCheck = False
        conddb.Tags["ONLINE"] = 'fake'
        conddb.IgnoreHeartBeat = True
        conddb.UseDBSnapshot = True

        from Configurables import EventSelector
        EventSelector().PrintFreq = 1000

        # Updated L0 decoding and filter
        # from Configurables import L0DUFromRawAlg
        # l0du_alg = L0DUFromRawAlg()
        from Configurables import LoKiSvc
        LoKiSvc().Welcome = False

        from Configurables import L0DUReportMonitor
        l0du_mon = L0DUReportMonitor()

        from Configurables import LoKi__L0Filter
        filter_code = "L0_DECISION_PHYSICS"
        # filter_code = "L0_DECISION_BEAM1 | L0_DECISION_BEAM2"
        l0_filter = LoKi__L0Filter('L0Filter', Code = filter_code)

        # Plug routing bits filter in before the L0 emulation sequence
        # from Configurables import HltRoutingBitsFilter
        # rb_filter = HltRoutingBitsFilter( "RoutingBitsFilter" )
        # rb_filter.RequireMask = [0x0, 0x4, 0x0]

        # HLT Bank killer
        from Configurables import bankKiller
        hlt_banks = [ 'HltDecReports','HltRoutingBits','HltSelReports','HltVertexReports','HltLumiSummary','HltTrackReports' ]
        killer = bankKiller( 'RemoveInputHltRawBanks',  BankTypes = hlt_banks )

        # ODIN filter
        from Configurables import LoKi__ODINFilter as ODINFilter
        from Configurables import createODIN
        odinFilter = ODINFilter("ODINPhysicsFilter", Code = "ODIN_TRGTYP == LHCb.ODIN.PhysicsTrigger")
        # odinFilter = ODINFilter("ODINPhysicsFilter", Code = "(ODIN_BXTYP == LHCb.ODIN.Beam1) | (ODIN_BXTYP == LHCb.ODIN.Beam2)")
        ApplicationMgr().TopAlg = [createODIN(), odinFilter, killer]
        # ApplicationMgr().TopAlg = [createODIN(), odinFilter, rb_filter, killer]

        # MDF writer
        from Configurables import GaudiSequencer
        seq = GaudiSequencer('WriterSeq')

        filename = 'RedoL0_' + app.TCK
        from Configurables import LHCb__MDFWriter as MDFWriter
        mdf_writer = MDFWriter('MDFWriter', Compress = 0, ChecksumType = 1, GenerateMD5 = True,
                               Connection = 'file://%s' % self.__output_file)

        # Sequence is an OutStream so it runs after the L0 emulation stuff
        seq.Members = [l0du_mon, l0_filter, mdf_writer]
        ApplicationMgr().OutStream = [seq]

        # Timing table to make sure things work as intended
        from Configurables import AuditorSvc, LHCbTimingAuditor
        ApplicationMgr().AuditAlgorithms = 1
        if 'AuditorSvc' not in ApplicationMgr().ExtSvc:
            ApplicationMgr().ExtSvc.append('AuditorSvc')
        AuditorSvc().Auditors.append(LHCbTimingAuditor(Enable = True))

        from GaudiPython import AppMgr
        gaudi = AppMgr()
        gaudi.initialize()
        gaudi.run(app.EvtMax)
        gaudi.stop()
        gaudi.finalize()

        self.done()

## Merge the files
def chunks(files, size = 1024):
    """ Yield successive n-sized chunks from l.
   """
    for r, l in files.iteritems():
        i = 0
        j = 0
        s = 0
        n = 0
        for j, (f, fs) in enumerate(l):
            s += fs
            if s > size * 1024 ** 2:
                s = 0
                n += 1
                yield l[i : j + 1], (r, n)
                i = j + 1
        yield l[i :], (r, n + 1)

# running = {i : False for i in range(len(mergers))}
# done = {i : False for i in range(len(mergers))}

# source_dir = '/localdisk/hlt1/nobias'
# files = defaultdict(set)
# re_file = re.compile(r"Run_0*([0-9]+)_.*\.(mdf|raw)")
# for f in os.listdir(source_dir):
#     m = re_file.match(os.path.split(f)[-1])
#     if m:
#         r = int(m.group(1))
#         files[r].add(f)

#db = shelve.open("filesMup.db")
db = shelve.open("filesMdown.db")

files = db['files']

tck = '0x1701'
dest_dir = '/localdisk1/bw_division/cofitzpa/2016NB_0x1701/{0}'.format(tck)
of_pat = os.path.join(dest_dir, '2016NB_{0}_%d_%02d.mdf'.format(tck))
files = {r : sorted(v) for r, v in files.iteritems()}
redoers = [Redoer(j, [f[0] for f in input_files], of_pat % i, tck) for j, (input_files, i) in enumerate(chunks(files, 15 * 1024))]
runner = Runner(redoers, max_run = 10)
runner.run_processes()
