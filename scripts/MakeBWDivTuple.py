import os, sys, re, shelve
from collections import defaultdict
from HltPiquetScripts.Parallel import Worker, Runner

class Redoer(Worker):
    def __init__(self, n, input_files, output_file, tck):
        super(Redoer, self).__init__(n)
        self.__input_files = input_files
        self.__output_file = output_file
        self.__written = 0
        self.__tck = tck

    def input_files(self):
        return self.__input_files
        
    def run(self):
        # General configuration
        from Gaudi.Configuration import ApplicationMgr
        from Configurables import DaVinci
        app = DaVinci()
        app.EvtMax = -1
        app.DataType = '2016'

        app().TupleFile = self.__output_file
        app().HistogramFile = ''

        from Configurables import LoKi__HDRFilter as StripFilter
        stripFilter = StripFilter( 'stripPassFilter',\
                Code = "HLT_PASS('StrippingMBNoBiasDecision')",\
                Location= "/Event/Strip/Phys/DecReports")
                app().EventPreFilters = [stripFilter]


        from DecayTreeTuple.Configuration import EventTuple
        etuple = EventTuple()
        etuple.addTupleTool("TupleToolL0Data")
        app.UserAlgorithms = [etuple]






#        app.CondDBtag = 'cond-20160522'
#        app.DDDBtag = 'dddb-20150724'

        from FileStager.Configuration import configureFileStager
        configureFileStager()
        
        from GaudiConf import IOHelper
        IOHelper("MDF", "MDF").inputFiles(self.__input_files)

        from Configurables import EventSelector
        EventSelector().PrintFreq = 1000

        filename = 'BWTuple_'



        # Timing table to make sure things work as intended
        from Configurables import AuditorSvc, LHCbTimingAuditor
        ApplicationMgr().AuditAlgorithms = 1
        if 'AuditorSvc' not in ApplicationMgr().ExtSvc:
            ApplicationMgr().ExtSvc.append('AuditorSvc')
        AuditorSvc().Auditors.append(LHCbTimingAuditor(Enable = True))

        from GaudiPython import AppMgr
        gaudi = AppMgr()
        gaudi.initialize()
        gaudi.run(app.EvtMax)
        gaudi.stop()
        gaudi.finalize()

        self.done()

## Merge the files
def chunks(files, size = 1024):
    """ Yield successive n-sized chunks from l."""
    for r, l in files.iteritems():
        i = 0
        j = 0
        s = 0
        n = 0
        for j, (f, fs) in enumerate(l):
            s += fs
            if s > size * 1024 ** 2:
                s = 0
                n += 1
                yield l[i : j + 1], (r, n)
                i = j + 1
        yield l[i :], (r, n + 1)

# running = {i : False for i in range(len(mergers))}
# done = {i : False for i in range(len(mergers))}

# source_dir = '/localdisk/hlt1/nobias'
# files = defaultdict(set)
# re_file = re.compile(r"Run_0*([0-9]+)_.*\.(mdf|raw)")
# for f in os.listdir(source_dir):
#     m = re_file.match(os.path.split(f)[-1])
#     if m:
#         r = int(m.group(1))
#         files[r].add(f)

db = shelve.open("files.db")
files = db['files']

dest_dir = '/localdisk1/hlt/cofitzpa/l0bw/'
of_pat = os.path.join(dest_dir, '2016NB_%d_%02d.root')
files = {r : sorted(v) for r, v in files.iteritems()}
redoers = [Redoer(j, [f[0] for f in input_files], of_pat % i, tck) for j, (input_files, i) in enumerate(chunks(files, 15 * 1024))]
runner = Runner(redoers, max_run = 10)
runner.run_processes()
