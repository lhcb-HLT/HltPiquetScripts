from operator import itemgetter
import os, sys, re
from collections import defaultdict
from HltPiquetScripts.Parallel import Worker, Runner
from HltPiquetScripts.SFTPClient import SFTPClient

class Merger(Worker):
    def __init__(self, n, input_files, output_file):
        super(Merger, self).__init__(n)
        self.__input_files = input_files
        self.__output_file = output_file
        self.__written = 0

    def run(self):
        from Configurables import LHCbApp
        app = LHCbApp()
        app.EvtMax = -1
        app.DataType = '2015'
        app.CondDBtag = 'cond-20150828'
        app.DDDBtag = 'dddb-20150724'
        # app.Simulation = True

        from FileStager.Configuration import configureFileStager
        configureFileStager()

        from Configurables import CondDB
        CondDB().Online = True

        from Configurables import EventSelector
        EventSelector().PrintFreq = 1000

        from GaudiConf import IOHelper
        IOHelper("MDF", "MDF").inputFiles(self.__input_files)

        # ODIN
        from Configurables import createODIN

        # Bank killer
        from Configurables import bankKiller
        hlt_banks = [ 'HltDecReports','HltRoutingBits','HltSelReports','HltVertexReports','HltLumiSummary','HltTrackReports' ]
        killer = bankKiller( 'RemoveInputHltRawBanks',  BankTypes = hlt_banks )

        # MDF writer
        from Configurables import LHCb__MDFWriter as MDFWriter
        mdf_writer = MDFWriter('MDFWriter', Compress = 0, ChecksumType = 1, GenerateMD5 = True,
                               Connection = 'file://%s' % self.__output_file)

        # L0DUReport
        from Gaudi.Configuration import importOptions
        importOptions("$L0TCK/L0DUConfig.opts")
        from Configurables import L0DUReportMonitor, L0DUFromRawAlg

        # ODIN filter
        from Configurables import LoKi__ODINFilter as ODINFilter
        odinFilter = ODINFilter("ODINPhysicsFilter", Code = "ODIN_TRGTYP == LHCb.ODIN.PhysicsTrigger")

        # Sequence
        from Configurables import GaudiSequencer
        seq = GaudiSequencer("Sequence", Members = [createODIN(), odinFilter, killer, L0DUFromRawAlg(), L0DUReportMonitor(), mdf_writer])
        from Gaudi.Configuration import ApplicationMgr
        ApplicationMgr().TopAlg = [seq]

        # Timing table to make sure things work as intended
        from Configurables import AuditorSvc, LHCbTimingAuditor
        ApplicationMgr().AuditAlgorithms = 1
        if 'AuditorSvc' not in ApplicationMgr().ExtSvc:
            ApplicationMgr().ExtSvc.append('AuditorSvc')
        AuditorSvc().Auditors.append(LHCbTimingAuditor(Enable = True))

        from GaudiPython import AppMgr
        gaudi = AppMgr()
        gaudi.initialize()
        gaudi.run(app.EvtMax)
        gaudi.stop()
        gaudi.finalize()

        self.done()

## Merge the files
def chunks(files, size = 1024):
    """ Yield successive n-sized chunks from l.
   """
    for r, l in files.iteritems():
        i = 0
        j = 0
        s = 0
        n = 0
        for j, (f, fs) in enumerate(l):
            s += fs
            if s > size * 1024 ** 2:
                s = 0
                n += 1
                yield l[i : j + 1], (r, n)
                i = j + 1
        yield l[i :], (r, n + 1)

# running = {i : False for i in range(len(mergers))}
# done = {i : False for i in range(len(mergers))}

source_dir = '/localdisk/L0Emulation'
files = defaultdict(set)

re_file = re.compile(r"Run_0*([0-9]+)_.*\.(mdf|raw)")
for node in ['storerecv%02d' % d for d in range(1, 5)]:
    with SFTPClient('raaij', node) as sftp:
        for f in sftp.listdir(source_dir):
            m = re_file.match(os.path.split(f)[-1])
            if m:
                r = int(m.group(1))
                f = os.path.join(source_dir, f)
                s = sftp.stat(f).st_size
                files[r].add(("PFN:ssh://%s:%s" % (node, f.encode('ascii')), s))

dest_dir = '/localdisk/hlt1/raaij/merged'
of_pat = os.path.join(dest_dir, '2015NB_25ns_0x1606_%d.mdf')
files = {r : sorted(v, key = itemgetter(0)) for r, v in files.iteritems()}
runner = Runner([Merger(j, [f[0] for f in input_files], of_pat % i[0]) for j, (input_files, i) in enumerate(chunks(files, 1024))])
runner.run_processes()
