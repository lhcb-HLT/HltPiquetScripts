import os, re
import Gaudi.Configuration
from Moore.Configuration import Moore
from GaudiKernel.SystemOfUnits import MeV, mm, m

## Adapt these to your local environment
# Moore().generateConfig = True
# Moore().configLabel = 'van der Meer Scan May 2015'

Moore().ThresholdSettings = 'Physics_25ns_October2015'
Moore().UseTCK = False
Moore().OutputLevel = 3
Moore().EvtMax = 2000
Moore().EnableTimer = 'timing.csv'
Moore().CheckOdin = False
Moore().Simulation = False
Moore().ForceSingleL0Configuration = False
Moore().CondDBtag = 'cond-20150828'
Moore().DDDBtag = 'dddb-20150724'

from Configurables import EventSelector
EventSelector().PrintFreq = 100

Moore().Split = ''

from Configurables import CondDB
CondDB().Simulation = False
CondDB().UseDBSnapshot = True
CondDB().DBSnapshotDirectory = "/group/online/hlt/conditions"
CondDB().EnableRunChangeHandler = True
CondDB().Tags["ONLINE"] = 'fake'
CondDB().Online = True

import sys
try:
    import All
except ImportError:
    rd = '/group/online/hlt/conditions/RunChangeHandler'
    sys.path.append(rd)
    import All

import All
CondDB().RunChangeHandlerConditions = {k.replace('2016', '2015') : v for k, v in All.ConditionMap.iteritems()}

from Configurables import MagneticFieldSvc
MagneticFieldSvc().UseSetCurrent = True

# from PRConfig.TestFileDB import test_file_db
# input = test_file_db['2015HLTValidationData_L0filtered_0x0050']
# input.run(configurable=Moore())
source_dir = '/scratch/hlt/hlt1'
runnrs = [167020]
re_file = re.compile(r"(Run_)?(0*%s)_.*\.(mdf|raw)" % ('|'.join((str(r) for r in runnrs))))
files = sorted([os.path.join(source_dir, f) for f in os.listdir(source_dir) if re_file.match(f)])
Moore().inputFiles = files

from Configurables import JemallocProfile
jp = JemallocProfile()
jp.StartFromEventN = 499
jp.StopAtEventN = 1999
jp.DumpPeriod = 100

from GaudiKernel.Configurable import applyConfigurableUsers
applyConfigurableUsers()

from Configurables import GaudiSequencer
GaudiSequencer("Hlt2MonitorSequence").Members += ['JemallocProfile/JemallocProfile']
