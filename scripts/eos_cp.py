#!/usr/bin/env python
import os
import sys
import shlex
import argparse
import subprocess
import re

parser = argparse.ArgumentParser(usage = 'usage: %(prog)s output_dir <lfn lfn lfn ...|(file|file.py|dir)>')

parser.add_argument("directory", nargs = 1)
parser.add_argument("lfns", nargs = '+')
parser.add_argument("-s", "--select", type = str, dest = "select", default = ".*")
parser.add_argument("--eos-server", type = str, dest = "eos_server", default = "eoslhcb.cern.ch:1094")

args = parser.parse_args()

try:
    from XRootD import client
except ImportError:
    print 'Trying to determine path to xrootd python bindings from location of xrdcp'
    p = subprocess.Popen(['which', 'xrdcp'], stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
    o, e = p.communicate()
    if 'no xrdcp in' in o:
        raise
    xrdcp = o.split('\n')[0].strip()
    base_dir = os.path.sep.join(xrdcp.split(os.path.sep)[:-2])
    if 'LCG' not in base_dir:
        print 'Cannot determine path to XRootD python bindings from the location of xrdcp'
        print 'xrdcp is not taken from LCG'
    vi = sys.version_info
    xrd_dir = os.path.join(base_dir, 'lib', 'python{}.{}'.format(vi.major, vi.minor), 'site-packages')
    sys.path.append(xrd_dir)
    from XRootD import client
    print 'Found xrootd bindings in', xrd_dir

class eos_wrapper(object):
    def __init__(self):
        self.__host = 'root://{}//'.format(args.eos_server)
        from XRootD.client.flags import DirListFlags, OpenFlags, MkDirFlags, QueryCode
        self.__xrd = client.FileSystem(self.__host)
        
    def prefix(self):
        return self.__host
        
    def mkdir(self, d):
        if self.exists(d):
            return
        from XRootD.client.flags import MkDirFlags
        status, info = self.__xrd.mkdir(d, MkDirFlags.MAKEPATH)
        return status.ok

    def exists(self, p):
        status, info = self.__xrd.stat(p)
        return status.ok and info

    def size(self, f):
        status, info = self.__xrd.stat(f)
        return info.size if status.ok and info else None

    def listdir(self, d):
        status, listing = self.__xrd.dirlist(d)
        if not status.ok:
            return []
        else:
            return [e.name for e in listing]

    def isfile(self, f):
        from XRootD.client.flags import StatInfoFlags
        status, info = self.__xrd.stat(f)
        return status.ok and not (info.flags & StatInfoFlags.IS_DIR)

class std_wrapper(object):
    def __init__(self):
        pass

    def prefix(self):
        return ''
    
    def mkdir(self, d):
        if self.exists(d):
            return
        os.makedirs(d)

    def exists(self, d):
        return os.path.exists(d)

    def size(self, f):
        s = os.stat(f)
        return s.st_size

    def listdir(self, d):
        return os.listdir(d)

    def isfile(self, f):
        return os.path.isfile(f)

lfns = {}
if len(args.lfns) == 1:
    path = args.lfns[0]
    if not os.path.exists(path):
        print 'File containing lfns %s, does not exist.' % path
        sys.exit(-1)
    if path.endswith('.py'):
        mod = path[:-3]
        if os.path.sep in mod:
            p, m = mod.rsplit('/', 1)
            sys.path.append(p)
            lfn_mod = __import__(m, globals(), locals())
            sys.path.remove(p)
        else:
            lfn_mod = __import__(mod, globals(), locals())
        lfns = lfn_mod.lfns
    elif os.path.isfile(path):
        lfns[''] = []
        with open(path) as f:
            for line in f:
                lfns[''].append(line.strip())
    elif os.path.isdir(path):
        lfns[''] = []
        for f in sorted(os.listdir(path)):
            lfns[''].append(os.path.join(path, f))
else:
    lfns[""] = args.lfns

output_dir = args.directory[0]
for fun in (os.path.expanduser, os.path.expandvars):
    output_dir = fun(output_dir)

eos = eos_wrapper()
local = std_wrapper()
if output_dir.startswith('/eos'):
    dest = eos
else:
    dest = local

copier = client.CopyProcess()

re_num = re.compile(r"_(\d+)$")
of_base = output_dir + '/' if not output_dir.endswith('/') else output_dir
for sd, l in lfns.iteritems():
    if not re.match(args.select, sd):
        continue
    dest_dir = of_base + sd
    dest.mkdir(dest_dir)
    dest_files = [os.path.join(dest_dir, f) for f in dest.listdir(dest_dir) if dest.isfile(os.path.join(dest_dir, f))]
    sizes = {df : dest.size(df) for df in dest_files}
    to_copy = []
    for lfn in l:
        source = eos if lfn.startswith('/eos') else local
        lfn_size = source.size(lfn)
        if lfn_size == None:
            print 'Source file does not exist:', lfn
            continue
        if lfn_size not in sizes:
            to_copy.append(lfn)
    n_re = re.compile('.*_(\\d+)\\..*')
    taken = set((int(n_re.match(lf).group(1)) for lf in sizes.iterkeys()))
    numbers = sorted(list(set(range(len(sizes) + len(to_copy))) - taken))
    for i, lfn in zip(numbers, to_copy):
        filename = lfn.rsplit(os.path.sep, 1)[-1]
        base, ext = filename.rsplit('.', 1)
        if ext in ('.log',):
            continue
        if re_num.search(base):
            of = re_num.sub('_%03d.' % i, base) + ext
        else:
            of = base + ('_%03d.' % i) + ext
        of = dest.prefix() + os.path.join(dest_dir, of)
        copier.add_job(lfn, of)

copier.prepare()
copier.run()
