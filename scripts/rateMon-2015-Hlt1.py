from ROOT import TH1F, TH2F,TFile, TProfile, TDatime,TTree
from os import environ, path
from Gaudi.Configuration import *
from Configurables import GaudiSequencer
from Configurables import LHCbApp
from math import *
from array import *
# #################################################
runnr = int(sys.argv[1])#89963#87967#89527#87967#88151#87967#87880
binsize = 1 # seconds per bin
N=-1# number of events (<0: take all)
#N=2000000

#datapath = './Run154873ForTrackerAlignment/'
#datapath = './Run154873ForMuonAlignment/'
#datapath = '/net/hlta0101/localdisk/Alignment/BWDivision/'
#datapath = '/net/hlta0101/localdisk/Alignment/Muon'
datapath = '/net/hltb0304/localdisk/hlt1/'
#datapath = '/localdisk/hlt1/data2015/0706NB/'
#datapath = "/daqarea/lhcb/data/2015/RAW/TURBO/LHCb/COLLISION15EM/"+str(runnr)+"/"
#datapath = "/daqarea/lhcb/data/2015/RAW/TURCAL/LHCb/COLLISION15EM/"+str(runnr)+"/"
#datapath  = "/daqarea/lhcb/data/2015/RAW/LUMI/LHCb/COLLISION15EM/"+str(runnr)+"/"

app = LHCbApp()
app.Simulation = False
app.DataType = '2015'
app.CondDBtag =  'cond-20150828'
app.DDDBtag   =  'dddb-20150724'
from Configurables import EventClockSvc
EventClockSvc().EventTimeDecoder = 'OdinTimeDecoder'

linesForL0 = { "Hadron"    : {"Location" : "/Event/Trig/L0/Calo",
                              "Type"     : 2  },
               "Electron"  : {"Location" : "/Event/Trig/L0/Calo",
                              "Type"     : 0  },
               "Photon"    : {"Location" : "/Event/Trig/L0/Calo",
                              "Type"     : 1  },
               "Muon"      : {"Location" : "/Event/Trig/L0/MuonCtrl"},
               "DiMuon"    : {"Location" : "/Event/Trig/L0/MuonCtrl"}}

linesForMassMon = {}
# Structure says to dig down for lines where top level is not a particle
linesForMassMon['Hlt1DiMuonHighMass'] = { 'HistoLimits' : [2900.,3300.,100],
                                          'Structure'   : [105.658,105.658]}
linesForMassMon['Hlt1CalibTrackingKPiDetached'] = { 'HistoLimits' : [1800.,1920.,100],
                                                    'Structure'   : [0.]}
linesForMassMon['Hlt2DiMuonJPsiTurbo'] = { 'HistoLimits' : [2900.,3300.,100],
                                                'Structure'   : [0.]}
linesForMassMon['Hlt2JPsiReFitPVsTurbo'] = { 'HistoLimits' : [2900.,3300.,100],
                                             'Structure'   : [0.]}
linesForMassMon['Hlt2PIDD02KPiTagTurboCalib'] = { 'HistoLimits' : [1850.,2150.,200],
                                                  'Structure' : [0.] }

linesForMassMon['Hlt2CharmHadD02KPi_XSecTurbo'] = { 'HistoLimits' : [1764.,1964.,100],
                                                    'Structure' : [0.] }

linesForMassMon['Hlt2CharmHadDst_2D0Pi_D02KPi_XSecTurbo'] = { 'HistoLimits' : [1850.,2150.,200],
                                                              'Structure' : [0.] }

linesForMassMon['Hlt2CharmHadDpm2KPiPi_XSecTurbo'] = { 'HistoLimits' : [1764.,1964.,100],
                                                       'Structure' : [0.] }

linesForMassMon['Hlt2CharmHadDs2KKPi_XSecTurbo'] = { 'HistoLimits' : [1864.,2064.,100],
                                                     'Structure' : [0.] }

massForNtuple           = []
candNumsForNtuple       = []
l0DecisionsForNtuple    = {}
hlt2Decisions           = {}
maxcands                = 30
lineNum                 = 0
outputFile = TFile('RateMonNtuple_'+str(runnr)+'.root','recreate')
outputTree = TTree('RateMonTriggerTree','The tree with all the trigger information')

#First the branches for the GEC info
nOT   = array('i',[0])
outputTree.Branch('nOT',nOT,'nOT/I')
nSPD  = array('i',[0])
outputTree.Branch('nSPD',nSPD,'nSPD/I')
nPV   = array('i',[0])
outputTree.Branch('nPV',nPV,'nPV/I')
pvMult = array('i',10*[0])
outputTree.Branch('pvMult',pvMult,'pvMult[nPV]/I')
for line in linesForMassMon :
    hlt2Decisions[line] = 0.
    candNumsForNtuple.append(array('i',[0]))
    massForNtuple.append(array('d',maxcands*[0]))
    outputTree.Branch('nCand_'+line,candNumsForNtuple[lineNum],'nCand_'+line+'/I')
    outputTree.Branch('Mass_'+line,massForNtuple[lineNum],'Mass_'+line+'['+'nCand_'+line+']/D')
    lineNum += 1

for line in linesForL0 :
    l0DecisionsForNtuple[line] = array('d',[0])
    outputTree.Branch(line,l0DecisionsForNtuple[line],line+'/D')

def fmt(name) :
    _fmt = { 'RAW' : "DATAFILE='PFN:%s' SVC='LHCb::MDFSelector'"
           , 'MDF' : "DATAFILE='PFN:%s' SVC='LHCb::MDFSelector'"
           , 'DST' : "DATAFILE='PFN:%s' TYP='POOL_ROOTTREE' OPT='READ'" 
           }
    return _fmt[ path.splitext(name)[-1].lstrip('.').upper() ] % name

def getRunDBRunInfo(run):
    # Read from the Run DB via the web and convert to python
    import urllib, json
    return json.loads( urllib.urlopen("http://lbrundb.cern.ch/api/run/"+str(run)).read() )

def timeFromRunDB( runnr ) :
    import GaudiPython,time
    # time.struct_time(tm_year=2012, tm_mon=3, tm_mday=30, tm_hour=12, tm_min=30, tm_sec=22, tm_wday=4, tm_yday=90, tm_isdst=-1)
    convert = lambda x : GaudiPython.gbl.Gaudi.Time( x.tm_year,x.tm_mon-1,x.tm_mday,x.tm_hour,x.tm_min,x.tm_sec,0L)
    info = getRunDBRunInfo(runnr)
    # data u'2012-03-30T12:30:22' does not match format '%a %b %d %H:%M:%S %Y'
    start,stop = time.strptime(info['starttime'],'%Y-%m-%dT%H:%M:%S'),time.strptime(info['endtime'],'%Y-%m-%dT%H:%M:%S')
    print info['starttime'],start,convert(start)
    print info['endtime'],stop,convert(stop)
    return convert(start), convert(stop)


#def timeFromRunDB( runnr ) :
#       import datetime,subprocess,os
#       import GaudiPython
#       convert = lambda x : GaudiPython.gbl.Gaudi.Time( x.year,x.month-1,x.day,x.hour,x.minute,x.second,0L)
#       os.environ['LD_LIBRARY_PATH']+=':/sw/oracle/10.2.0.4/linux64/lib'
#       start,stop = eval(subprocess.Popen(["./rateMon-2012-RunDBHelper.py","%s"%runnr],stdout=subprocess.PIPE).communicate()[0])[0]#/home/graven/run.py","%s"%runnr],stdout=subprocess.PIPE).communicate()[0])[0]
#       return convert(start), convert(stop)

def configureInput( runnr ) :
    import os, os.path
    tempinput = []
    #for farm in ['a','b','c','d','e'] :
    #    for subfarm in range(1,10) :
    #        for node in range(1,29) :
    #           thisnode = '/net/hlt'+farm+'0'+str(subfarm)
    #           if node < 10 :
    #               thisnode += '0'
    #           thisnode += str(node) + "/"
    thisnode = datapath
    dirlisting = os.listdir(thisnode)
    for f in dirlisting :
        if f.find(str(runnr)) > -1 :
            tempinput.append(fmt( os.path.join(thisnode,f) ))
    #dirname = os.path.join( '/localdisk/hlt1/Run_', '%s' % runnr )
    #dirname = os.path.join( '/daqarea/lhcb/data/2010/RAW/FULL/LHCb/COLLISION10', '%s' % runnr )
    #dirname = os.path.join( '/daqarea/lhcb/data/2010/RAW/FULL/FEST/FEST', '%s' % runnr )
    #dirname = os.path.join( '/daqarea/lhcb/data/2010/RAW/FULL/LHCb/BEAM1', '%s' % runnr )
    #tempinput = [ fmt( os.path.join(dirname,f) ) for f in sorted( os.listdir( dirname ) ) ] 
    EventSelector().Input = tempinput#[0:len(tempinput)]
    EventSelector().PrintFreq = 10000
    #print EventSelector()

def configureRawData() :
    #importOptions('$GAUDIPOOLDBROOT/options/GaudiPoolDbRoot.opts')
    importOptions('$STDOPTS/DecodeRawEvent.py')
    EventPersistencySvc().CnvServices.append( 'LHCb::RawDataCnvSvc' )
    from Configurables import  DataOnDemandSvc
    #DataOnDemandSvc().AlgMap['Hlt1/DecReports'] =  "Hlt1DecReportsDecoder"
    #DataOnDemandSvc().AlgMap['Hlt1/SelReports'] =  "Hlt1SelReportsDecoder"
    ApplicationMgr().ExtSvc.append( 'DataOnDemandSvc' )

first,last = None,None
first,last = timeFromRunDB( runnr )
print '%s -- %s ' % ( first.format(True,'%F %H:%M:%S'), last.format(True,'%F %H:%M:%S') )

configureInput(runnr)
configureRawData()
rootfile = 'rateMon_%s.root' % runnr

from Configurables import CondDB
conddb = CondDB()
conddb.IgnoreHeartBeat = True

conddb.Online = True
conddb.UseDBSnapshot = True
conddb.EnableRunChangeHandler = True
conddb.Tags['ONLINE'] = 'fake'
#import All
#conddb.RunChangeHandlerConditions = All.ConditionMap

conddb.EnableRunStampCheck = False

import GaudiPython
g = GaudiPython.AppMgr()

#############################

def events(g,n=-1) :
    i = 0
    evt = g.evtsvc()
    while True :
        if (n>0 and i==n) : break
        i += 1
        status  = g.run(1)
        if not status.isSuccess() :  break # end of file not recognized this way...
        if not evt['DAQ'] : break
        yield evt


def requireRoutingBits( rawEvent, w ) :
      rbb = rawEvent.banks(53)# why does LHCb.RawBank.BankType.HltRoutingBits not work?
      if rbb.size()==0 : return False
      rb = rbb[0].data()
      for i in [ 0,1,2] :
          if w[i] and ( rb[i]&w[i] == 0 ) : return False
      return True
def isPhysics( rawEvent ) :
      return requireRoutingBits( rawEvent, [ 0x0,0x4,0x0 ] )



runevent = {}
def checkDup(odin) :
    run = odin.runNumber()
    if run not in runevent : runevent[ run ] = set()
    evtnr = odin.eventNumber()
    duplicate = ( evtnr in runevent[run] )
    runevent[run].add( evtnr )
    return duplicate

def algorithms() :
    import GaudiPython
    g = GaudiPython.AppMgr()
    for name in g.algorithms() : yield g.algorithm(name)

def rewind() :
    import GaudiPython
    g = GaudiPython.AppMgr()
    enabled = [ alg for alg in algorithms() if alg.Enable ]
    for alg in enabled : alg.Enable = False
    g.evtsel().rewind() 
    for alg in enabled : alg.Enable = True

def firstlastEventTime(g,N) :
    first = None
    last = None
    for evt in events(N,g) :
        odin  = evt['DAQ/ODIN']
        t = odin.eventTime()
        if not first or  t.ns() < first.ns() : first = t
        if not last  or  t.ns() > last.ns()  : last = t
    rewind()
    return (first,last)


############################# first iteration over data: find first/last event
if not first : (first,last) = firstlastEventTime(g,N)

time = lambda x :  x.ns()/1e9 - 3600 # Daylight saving time correction...
#time = lambda x :  x.ns()/1e9 
interval = time(last) - time(first)
print '%s -- %s : %s ' % ( first.format(True,'%F %H:%M:%S'), last.format(True,'%F %H:%M:%S')  ,interval )
hists = {}
def _fill(name,t) :
    if name not in hists : 
        nbins = int((interval)/binsize)+1
        hists[name] = TH1F(name,'Rate(%s)'%name,nbins,0,nbins*binsize) 
        x = hists[name].GetXaxis()
        x.SetTimeDisplay(1)
        x.SetTimeOffset(time(first))
        x.SetTimeFormat("#splitline{%Y/%m/%d}{%H:%M:%S}");
        x.SetLabelSize(0.03);
        x.SetLabelOffset(0.04);
        hists[name].GetYaxis().SetTitle("Average Rate (Hz)")
    hists[name].Fill( time(t)-time(first), float(1)/binsize) 
    #print '%s : %s ' % (name, time(t)-time(first))

hists2 = {}
hists4 = {}
def _fillNCand(name,n) :
    name = 'cand_%s' % name
    if (name not in hists2 ) and (name not in hists4): 
        if name.find("SPD") > -1 :
                hists4[name] = TH2F(name,'#Candidates(%s)'%name,1200,-0.5,1199.5,200,0,20000)
        else :
                hists2[name] = TH1F(name,'#Candidates(%s)'%name,120,-0.5,119.5)
    if name.find("SPD") > -1 :
        hists4[name].Fill( n[0],n[1])
    else :
        hists2[name].Fill( n ) 

hists3 = {}
def _createHistosForMCand(name) :
    hname = 'mass_%s' % name
    if hname not in hists3 :
        hists3[hname] = TH1F(hname,'CandMass(%s)'%name, linesForMassMon[name]['HistoLimits'][2],
                                                        linesForMassMon[name]['HistoLimits'][0],
                                                        linesForMassMon[name]['HistoLimits'][1])
def _fillMCand(name,n) :
    hists3['mass_'+name].Fill( n )

for i in linesForMassMon : 
    _createHistosForMCand(i)

############################### second iteration: fill histograms
for evt in events(g,N) :
    continue
    ottool = g.toolsvc().create('OTRawBankDecoder',interface='IOTRawBankDecoder')
    odin  = evt['DAQ/ODIN']
    t = odin.eventTime()
    #print odin.triggerType()
    #print odin
    #print t.format(True,'%F %H:%M:%S')
    _fill('everything',t)

    raw = evt['DAQ/RawEvent']
    rbb = raw.banks(53)# why does LHCb.RawBank.BankType.HltRoutingBits not work?
    if rbb.size()!=0 : 
      rb = rbb[0].data()
      for i in [ k for k in range(3) if rb[k] ] :
        [ _fill('RoutingBit%02d'%(i*32+j),t) for j in range(32) if rb[i]&(0x1<<j) ]
    else :
      print 'no routing bank!'

    if not isPhysics(raw) :
          _fill('isLumiExclusive',t)
          continue
    _fill('isNotLumiExclusive',t)

    hdr1   = evt['Hlt1/DecReports']
    [ _fill(i,t) for i in hdr1.decisionNames() if hdr1.decReport(i).decision() ]

    hdr2   = evt['Hlt2/DecReports']
    [ _fill(i,t) for i in hdr2.decisionNames() if hdr2.decReport(i).decision() ]

    for line in linesForMassMon :
        if hdr2.decReport(line+'Decision') :
            if hdr2.decReport(line+'Decision').decision() :
                hlt2Decisions[line] += 1

    l0du  = evt['Trig/L0/L0DUReport']
    [ _fill('L0'+k,t) for k in ['PU','PU20','CALO','SPD','SPD40' ] if l0du.channelDecisionByName(k) ]

    hsr1 = evt['Hlt1/SelReports']
    [ _fillNCand(i, hsr1.selReport(i).substructure().size()  ) for i in hsr1.selectionNames() ]

    hsr2 = evt['Hlt2/SelReports']

    try :
        if (evt['Hlt1/DecReports'].decReport("Hlt1MBNoBiasDecision").decision() ):
                _fillNCand("SPDMult",(evt['Raw/Spd/Digits'].size(),ottool.totalNumberOfHits()))
    except :
        fail = True

    nOT[0]  = ottool.totalNumberOfHits()
    nSPD[0] = evt['Raw/Spd/Digits'].size()
    nPV[0] = 0 
    if evt['/Event/Hlt1/VertexReports'].vertexReport('PV3D') :
        nPV[0] = evt['/Event/Hlt1/VertexReports'].vertexReport('PV3D').size()
        i = 0
        if nPV[0] == 0 :
            for i in hdr2.decisionNames() :
                if hdr2.decReport(i).decision() : 
                    print "Event with 0 PVs (VertexReport exists) selected by line",i 
        for pv in evt['/Event/Hlt1/VertexReports/PV3D'] :
            pvMult[i] = (pv.nDoF() + 3)/2
            i += 1
    else :
        for i in hdr2.decisionNames() :
            if hdr2.decReport(i).decision() :
                print "Event with 0 PVs (Vertex report does NOT exist) selected by line",i

    maxmuon = -999.
    secondmuon = -999.
    for line in linesForL0 :
        l0DecisionsForNtuple[line][0] = -999. 
        if line == "DiMuon" : continue
        thislocation = linesForL0[line]["Location"]
        if line.find("Muon") > -1 :
          for entry in evt[thislocation] :
              thiscandpt = entry.pt()
              if thiscandpt > maxmuon : 
                  if maxmuon > 0. : 
                      secondmuon = maxmuon
                  maxmuon = thiscandpt
              elif thiscandpt > secondmuon :
                  secondmuon = thiscandpt
        else :
          thistype     = linesForL0[line]["Type"]
          for entry in evt[thislocation] :
              if entry.type() == thistype :
                  l0DecisionsForNtuple[line][0] = entry.et()
                  break
    l0DecisionsForNtuple["Muon"][0] = maxmuon
    if maxmuon > 0. and secondmuon > 0. :
        l0DecisionsForNtuple["DiMuon"][0] = maxmuon*secondmuon
    else : 
        l0DecisionsForNtuple["DiMuon"][0] = -999.

    lineNum = 0
    for i in linesForMassMon :
        candNumsForNtuple[lineNum][0] = 0
        for tloop in range(0,maxcands) :
            massForNtuple[lineNum][tloop] = 0.
        hsr = hsr2 if i.find('Hlt2') > -1 else hsr1
        hdr = hdr2 if i.find('Hlt2') > -1 else hdr1        

        if not hdr.decReport(i+"Decision") : 
            lineNum += 1
            continue
        if not hdr.decReport(i+"Decision").decision() : 
            lineNum += 1
            continue

        if hsr.selReport(i+"Decision") :
            #print "I have a decision for",i
            if hsr.selReport(i+"Decision").substructure() :
                for cand in hsr.selReport(i+"Decision").substructure() :
                    #print cand.substructure()[0].lhcbIDs()
                    mass = 0.
                    if len(linesForMassMon[i]['Structure']) == 1 :  
                        mass = cand.numericalInfo()["1#Particle.measuredMass"]
                    else : # have to dig down
                        index = -1
                        cand_px = 0.
                        cand_py = 0.
                        cand_pz = 0.
                        cand_E  = 0.
                        for child in cand.substructure() :
                            childtrack = child
                            while True :
                                if len(childtrack.numericalInfo()) == 0 :
                                    childtrack = childtrack.substructure()[0]
                                    else :
                                        break
                            index += 1 
                            thismass = linesForMassMon[i]['Structure'][index]
                            tx = childtrack.numericalInfo()[ "3#Track.firstState.tx" ]
                            ty = childtrack.numericalInfo()[ "4#Track.firstState.ty" ]
                            p  = fabs(1./childtrack.numericalInfo()[ "5#Track.firstState.qOverP" ])
                            normfactor = sqrt(tx*tx+ty*ty+1)
                            cand_px += tx*p/normfactor
                            cand_py += ty*p/normfactor
                            cand_pz += 1.*p/normfactor
                            cand_E  += sqrt(thismass*thismass + p*p)
                        m2 = cand_E*cand_E - cand_px*cand_px - cand_py*cand_py - cand_pz*cand_pz
                        mass = -999 if (m2 < 0) else sqrt(m2)
                    _fillMCand(i,mass)
                    if (candNumsForNtuple[lineNum][0] < maxcands) and (mass < 100000.) :
                        massForNtuple[lineNum][candNumsForNtuple[lineNum][0]] = mass
                        candNumsForNtuple[lineNum][0] += 1
                        
        lineNum += 1
    
    outputTree.Fill()

outputTree.Write()
 
for h in hists.itervalues() : h.Write()
for h in hists2.itervalues() : h.Write()
for h in hists3.itervalues() : h.Write()
for h in hists4.itervalues() : h.Write()
outputFile.Close()

print hlt2Decisions
