from operator import itemgetter
from itertools import product
import os, sys, re, atexit
from collections import defaultdict
from multiprocessing import Process, Queue
from HltPiquetScripts.SFTPClient import SFTPClient
import shelve

total = 0

db = shelve.open('files.db')
if 'files' in db:
    nodes = db['nodes']
    files = db['files']
    for v in files.itervalues():
        for e in v:
            total += e[1]
else:
    files = defaultdict(set)
    nodes = set()

def write_files():
    db['nodes'] = nodes
    db['files'] = files
    db.close()

# import atexit
# atexit.register(write_files)

source_dir = '/localdisk/Alignment/Calo'
runnrs = [178319, 178327, 178339, 178436, 178469]
re_file = re.compile(r"Kali-hlt[a-f][0-9]{4}-[0-9]_(default|beat)\.fm(?:DST|dst)")
# re_file = re.compile(r"(?:Run_)?(0*(%s)).*\.(mdf|raw)" % ('|'.join((str(r) for r in runnrs))))

def get_files(node, queue):
    hostname = 'hlt%s%02d%02d' % node
    try:
        files = defaultdict(set)
        total = 0
        def done():
            queue.put((hostname, None, None))

        with SFTPClient('raaij', hostname) as sftp:
            if not sftp:
                print "Could not connect to %s" % hostname
                return done()
            try:
                contents = sftp.listdir(source_dir)
            except IOError as e:
                return done()
            if not contents:
                return done()
            for f in contents:
                m = re_file.match(os.path.split(f)[-1])
                if m:
                    try:
                        r = int(m.group(1))
                    except IndexError:
                        r = 0
                    except ValueError:
                        r = m.group(1)
                    f = os.path.join(source_dir, f)
                    s = None
                    try:
                        s = sftp.stat(f).st_size
                    except IOError as e:
                        print hostname, e
                        continue
                    total += s
                    files[r].add(("PFN:ssh://%s:%s" % (hostname, f.encode('ascii')), s))
        queue.put((hostname, float(total), files))
        return
    except KeyboardInterrupt:
        done()
    print '%s exited' % hostname

queue = Queue()

processes = {}
target = 250 * 1024 ** 3

todo = [n for n in product(['a', 'b', 'c', 'd', 'e', 'f'], range(1, 11), range(5, 29))]
done = set()
i = 0

try:
    while len(done) < (len(todo) - 1):
        while len(processes) < 10:
            i += 1
            if i >= len(todo):
                break
            node = todo[i]
            hostname = 'hlt%s%02d%02d' % node
            if hostname in nodes:
                print hostname, 'already done'
                done.add(hostname)
                continue
            p = Process(target = get_files, args = (node, queue))
            p.start()
            processes[hostname] = p
            if len(processes) >= 10:
                break

        msg = queue.get()
        hostname, t, f = msg
        if t:
            total += t
            print hostname, total / float(1024 ** 2)
            for r, s in f.iteritems():
                files[r].update(s)
            nodes.add(hostname)
        if hostname in processes:
            p = processes.pop(hostname)
            p.join()
        done.add(hostname)
        if total > target:
            break
except KeyboardInterrupt:
    pass

write_files()
for p in processes.itervalues():
    p.join()
