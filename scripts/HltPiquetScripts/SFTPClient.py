from time import sleep
import os, sys, re, select
from random import shuffle
from multiprocessing import Process, Queue
import socket, sys
from itertools import product
from collections import defaultdict
from HltPiquetScripts.Parallel import Worker
import zmq

class SFTPClient(object):
    def __init__(self, username, hostname, ssh_port = 22):
        self.hostname = hostname
        self.username = username
        self.port = ssh_port

        self.__closed = False
        self.__client = None
        self.__sftp = None

    def __prepare_client(self):
        if self.__client != None:
            return self

        import paramiko
        from paramiko.ssh_exception import SSHException, NoValidConnectionsError

        try:
            socket.getaddrinfo(self.hostname, self.port)
        except socket.gaierror:
            print 'Could not get hostname %s' % self.hostname
            self.__transport = [False, None]
            return

        agent = paramiko.Agent()
        agent_keys = agent.get_keys()
        if len(agent_keys) == 0:
            print 'No keys'
            self.__client = [False, None]
            return

        client = None
        try:
            client = paramiko.SSHClient()
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            client.load_system_host_keys()
            client.connect(self.hostname, port = self.port, timeout = 2)
            self.__client = [True, client]
            return
        except SSHException as e:
            print 'No Auth'
            print e
            self.__client = [False, None]
            return
        except NoValidConnectionsError as e:
            print 'No Connection'
            print e
            self.__client = [False, None]
            return
        except socket.timeout as e:
            print self.hostname, e
            self.__client = [False, None]
            return
        
    def __enter__(self):
        self.__prepare_client()
        return self.sftp()

    def sftp(self):
        self.__prepare_client()
        if not self.__client[0]:
            return

        if self.__sftp != None:
            return self.__sftp
        self.__sftp = self.__client[1].open_sftp()
        return self.__sftp

    # def list_dir(self, d):
    #     sftp = self.sftp()
    #     if not sftp:
    #         return

    #     ld = []
    #     try:
    #         ld = sftp.listdir(d)
    #     except IOError:
    #         pass
    #     return ld

    # def stat(self, f):
    #     return self.sftp().stat(f)

    # def get(self, s, d):
    #     return self.sftp().get(s, d)

    # def remove(self, f):
    #     return self.sftp().remove(f)

    def __exit__(self, type, value, traceback):
        if not self.__closed:
            if self.__client and self.__client[0]:
                self.__client[1].close()
            self.__closed = True


class SFTPCopier(Worker):
    def __init__(self, index, username, hostname, runs,
                 source_dir = '/localdisk/Alignment/BWDivision',
                 dest_dir = '/localdisk/hlt1/nobias', ssh_port=22):
        super(SFTPClient, self).__init__(index, args = [runs, source_dir, dest_dir])
        self.hostname = hostname
        self.username = username
        self.port = ssh_port

        self.__closed = False
        self.__transport = None

    def run(self, runs, source_dir, dest_dir):
        # Request what to do:
        # - "stop": stop process
        # - "no" : do not retrieve file
        # - "yes" :  retrieve file
        req = self.context().socket(zmq.REQ)
        req.connect("ipc://%s" % os.path.join(self.tmpdir(), "copy_data"))

        with SFTPClient(self.hostname, self.username, self.port) as sftp:

            runs = set(runs)
            retrieved = []
            if not (runs and sftp):
                print '%04d Could start SFTP client' % self.index()
                self.done()
                self.__close()
                return

            ## Start up an sftp client
            ld = sftp.list_dir(source_dir)

            ## List the files in the remote dir and find the ones we want
            re_file = re.compile(r"Run_0*([0-9]+)_.*\.(mdf|raw)")
            files = defaultdict(list)

            for f in ld:
                f = f.encode('ascii')
                m = re_file.match(f)
                if not m:
                    continue
                files[int(m.group(1))].append(f)

            if not files:
                print 'Could not find files in %s on %s' % (source_dir, self.hostname)

            failures = 0
            stop = False
            for r, to_get in files.iteritems():
                if r not in runs:
                    continue

                for f in sorted(to_get):
                    last = False
                    source = os.path.join(source_dir, f)
                    dest = os.path.join(dest_dir, f)

                    ## Get the size of the remote file to inquire if we need to get it
                    size = None
                    try:
                        st = sftp.stat(source)
                        size = float(st.st_size) / (1024 ** 2)
                    except SSHException:
                        print 'Failed to stat %s' % dest
                        failures += 1
                        if failures > 5:
                            stop = True
                            break

                    req.send_pyobj((r, f, size))
                    rep = req.recv()
                    if rep == 'stop':
                        stop = True
                        break
                    elif rep == 'last':
                        last = True
                    elif rep == 'no':
                        continue
                    elif rep != 'yes':
                        print "Bad reply, stopping"
                        stop = True
                        break

                    ## Get the file with sftp
                    try:
                        sftp.get(source, dest)
                        print '%04d Retrieved  %s' % (self.index(), source)
                        self.publish('file', (f, size))
                        if last:
                            stop = True
                            break
                    except SSHException:
                        print 'Failed to retrieve %s' % source
                        failures += 1
                        if failures > 5:
                            stop = True
                            break
                if stop:
                    break

            self.done()
