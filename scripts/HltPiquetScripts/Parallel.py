import os, sys, re, atexit
import socket
import zmq
from time import sleep
from itertools import product
from collections import defaultdict
from multiprocessing import Process

_tmpdir = None

def __remove_tmpdir():
    if not _tmpdir:
        return
    import shutil
    if os.path.exists(_tmpdir) and os.path.isdir(_tmpdir):
        shutil.rmtree(_tmpdir)

atexit.register(__remove_tmpdir)

def get_tmpdir():
    global _tmpdir
    if not _tmpdir:
        import tempfile
        _tmpdir = tempfile.mkdtemp(prefix = "copy_")
    return _tmpdir

class Runner(object):
    ## Class that manages running multiple processes in parallel. A maximum
    ## number can be launched and new ones will be started as available and
    ## needed.

    ## Worker processes are assumed to send a message when they are done or on
    ## error:
    ## context = zmq.Context()
    ## pub = context.socket(zmq.PUB)
    ## pub.connect("ipc://%s" % os.path.join(get_tmpdir(), "status"))
    ## pub.send_pyobj(((self.index(), "status"), "done"))

    context = zmq.Context()

    def __init__(self, processes, handlers = {}, max_run = 10):
        self.__max_run = max_run
        self.__processes = processes
        self.__handlers = handlers

        self.__poller = zmq.Poller()
        for socket, hs in handlers.iteritems():
            for t in hs.iterkeys():
                self.__poller.register(socket, t)

    def tmpdir(self):
        return get_tmpdir()

    def stop(self, message):
        return False

    def run_processes(self):
        # Listen to status publication of workers
        sub = Runner.context.socket(zmq.SUB)
        sub.bind("ipc://%s" % os.path.join(self.tmpdir(), "status"))
        sub.setsockopt(zmq.SUBSCRIBE, "")

        # The poller to allow listening to additional sockets too
        self.__poller.register(sub, zmq.POLLIN)

        # Keep track of who's running and who's done
        running = {i : False for i in range(len(self.__processes))}
        done = {i : False for i in range(len(self.__processes))}

        # How many do we run
        n_run = self.__max_run if len(self.__processes) > self.__max_run else len(self.__processes)

        # If we should already stop, don't even do anything
        if self.stop(""):
            return

        # Start the initial processes
        for i in range(n_run):
            running[i] = True
            self.__processes[i].start()

        stop = False
        # Main loop
        # Keep running while processes are still left
        # If we should stop, at least no running processes should be around
        while not all(done.values()) and not (stop and all([not r for r in running.itervalues()])):
            socks = dict(self.__poller.poll())
            if sub in socks and socks[sub] == zmq.POLLIN:
                # handle statuses
                (index, msg_type), message = sub.recv_pyobj()
                if msg_type == 'status' and message in ('done', 'error'):
                    done[index] = True
                    running[index] = False
                    self.__processes[index].join()
                else:
                    if self.stop(message):
                        stop = True

            # Dispatch rest of messages to registered handlers
            for socket, handlers in self.__handlers.iteritems():
                if socket in socks and socks[socket] in handlers:
                    handlers[socks[socket]]()

            # Start extra processes if needed
            for m in self.__processes:
                i = m.index()
                if done[i] or running[i]:
                    continue
                if not stop and sum(running.itervalues()) < self.__max_run:
                    m.start()
                    running[i] = True

class Worker(object):
    def __init__(self, index, args = []):
        self.__index = index
        if args:
            self.__process = Process(target = self.run_process, args = args)
        else:
            self.__process = Process(target = self.run_process)
        self.__context = None

    def context(self):
        return self.__context

    def index(self):
        return self.__index

    def tmpdir(self):
        return get_tmpdir()

    def run_process(self, *args):
        self.__context = zmq.Context()
        # Status publishing socket
        self.__pub = self.context().socket(zmq.PUB)
        self.__pub.connect("ipc://%s" % os.path.join(self.tmpdir(), "status"))
        self.run(*args)

    def run(self):
        self.done()

    def publish(self, msg_type, msg):
        self.__pub.send_pyobj(((self.index(), msg_type), msg))

    def done(self):
        sleep(0.5)
        self.publish('status', 'done')
        sleep(0.5)

    def error(self):
        sleep(0.5)
        self.publish('status', 'error')
        sleep(0.5)

    def process(self):
        return self.__process

    def start( self ):
        self.__process.start()

    def join( self ):
        self.__process.join()
