# minimal Gaudipython script to inspect the HLT selReports and decReports
from os import environ, path
from Gaudi.Configuration import *
from Configurables import GaudiSequencer
from Configurables import LHCbApp

# the top level configurable is LHCbApp
app = LHCbApp()
app.Simulation = False
app.DataType = '2015'
app.CondDBtag =  'cond-20150828'
app.DDDBtag   =  'dddb-20150724'

# sequence for decoding the HltDecReports
mySeq = GaudiSequencer("mySeq")
from DAQSys.Decoders import DecoderDB as ddb
from DAQSys.DecoderClass import decodersForBank
mySeq.Members+=[d.setup() for d in decodersForBank(ddb,"HltDecReports")]

# input data
from GaudiConf import IOHelper
# if running from private TCK:
#from Configurables import ConfigCDBAccessSvc
#ConfigCDBAccessSvc().File = '/afs/cern.ch/work/m/mvesteri/public/2016_TRIGGER_COMMISIONING/TCK130416/TCKData/config.cdb'
#IOHelper('MDF').inputFiles(['PFN:/afs/cern.ch/work/m/mvesteri/public/2016_TRIGGER_COMMISIONING/TCK130416/TestTCK2Turbo.mdf'])
# on the online cluster
IOHelper('MDF').inputFiles(['PFN:/scratch/hlt/make_tcks/FULL_164699_0000000090.raw'])

# set up the event loop
import GaudiPython
ApplicationMgr().TopAlg+=[mySeq]
gaudi = GaudiPython.AppMgr()
evt = gaudi.evtsvc()

# the event loop itself
while True:
    # process an event
    gaudi.run(1)
    
    # odin banks
    odin  = evt['DAQ/ODIN']
    if not odin: break
    print odin
    
    # hlt1 dec reports
    hdr1   = evt['Hlt1/DecReports']
    print [ i for i in hdr1.decisionNames() if hdr1.decReport(i).decision() ]
    
    # hlt2 dec reports
    #hdr2   = evt['Hlt2/DecReports']
    #print [ i for i in hdr2.decisionNames() if hdr2.decReport(i).decision() ]

gaudi.finalize()
