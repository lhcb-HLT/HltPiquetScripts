#!/usr/bin/env python
#
# Script to use several processes to write events with differing Hlt Decisions to a file

# MultiProcessing
from multiprocessing import Process, Queue, Event, Condition, Lock
from time import sleep, time
from copy import copy, deepcopy
import os
import sys
import pprint
import select
from collections import defaultdict
import random, optparse, subprocess

# Gaudi configuration
from Gaudi.Configuration import *
from GaudiPython import AppMgr, SUCCESS, FAILURE

def Splitter(files,nprocesses):
    SplitData = [[] for x in range(0,nprocesses)]
    j = -1
    for i in range(0,len(files)):
        j += 1
        SplitData[j].append(files[i])
        if j == nprocesses-1: j = -1
    return SplitData

class Task( object ):
    def __init__( self, name ):
        self._name = name

    def configure( self ):
        pass

    def initialize( self ):
        pass

    def run( self ):
        pass

    def finalize( self ) :
        pass

class ProcessData( object ):

    def __init__( self ):
        self._condition = Condition( Lock() )
        self._inQueue = Queue(100)
        self._outQueue = Queue(100)

    def getData( self, block = True, timeout = None ):
        try:
            self._data = self._outQueue.get( block, timeout )
            return self._data
        except IOError:
            return None
        
    def data( self ):
        return self._data

    def putData( self, data ):
        self._inQueue.put( data )

class ProcessWrapper( object ):

    def __init__( self, key, task, name, config, debug = False ):
        self._name = name
        self._key = key
        self._debug = debug
        self._name = name
        self._data = ProcessData()
        self._task = task( name, ( self._data._inQueue, self._data._outQueue ),
                           self._data._condition )
        self._config = config
        self._process = Process( target = self.run )

    def run( self):
        if self._debug == False:
            filename = '%s.log' % self._name
            if 'Run' in self._config:
                run = self._config[ 'Run' ]
                cwd = os.path.realpath( os.path.curdir )
                dirname = os.path.join( cwd, '%d' % run )
                filename = os.path.join( dirname, filename )
            old_stdout = os.dup( sys.stdout.fileno() ) 
            fd = os.open( filename, os.O_CREAT | os.O_WRONLY ) 
            os.dup2( fd, sys.stdout.fileno() ) 

        self._task.configure( self._config )
        self._task.initialize()
        self._task.run()
        self._task.finalize()

        if self._debug == False:
            os.close( fd ) 
            os.dup2( old_stdout, sys.stdout.fileno() )

    def name( self ):
        return self._name

    def key( self ):
        return self._key

    def start( self ):
        self._process.start()

    def join( self ):
        self._process.join()

    def data( self ) :
        return self._data.data()

    def condition( self ):
        return self._data._condition

    def fileno( self ):
        return self._data._outQueue._reader.fileno()

    def getData( self, block = True, timeout = None ):
        return self._data.getData( block, timeout )

    def putData( self, data ):
        self._data.putData( data )

class DecisionReporter( Task ):
    def __init__( self, name, queues, condition ):
        Task.__init__( self, name )
        self._config = dict()
        self._name = name
        self._inQueue = queues[ 0 ]
        self._outQueue = queues[ 1 ]
        self._condition = condition

        self.__algorithms = {}
        
    def configure( self, configuration ):
        
        # the top level configurable is Moore()
        from Configurables import Moore, HltConf
        moore = Moore()
        hltConf = HltConf()
        # configure the properties of Moore
        for ( attr, value ) in configuration.items():
            if attr in moore.getDefaultProperties():
                moore.setProp(attr, value)
            elif attr in hltConf.getDefaultProperties():
                hltConf.setProp(attr, value)
            self._config[ attr ] = value
        
        # input data
        from GaudiConf import IOHelper
        IOHelper('MDF').inputFiles(self._config[ 'Input' ])
        EventSelector().PrintFreq = 100
        
        # configure the output file
        moore.outputFile = self._name+".mdf"
        print hltConf
        print moore

    def initialize( self ):
        # initialise the top level configurable
        self._appMgr = AppMgr()
        self._appMgr.initialize()

        from Gaudi.Configuration import allConfigurables
        from HltLine.HltLine import hlt2Lines, hlt1Lines
        names = list(l.name() for l in hlt1Lines()) + list(l.name() for l in hlt2Lines())
        for line in names:
            if line.endswith('Global'):
                continue
            seq = line + 'FilterSequence'
            if seq not in allConfigurables:
                continue
            conf = allConfigurables[seq]
            if conf.Members[-1].name().endswith('Decision') and len(conf.Members) > 1:
                last = -2
            else:
                last = -1

            name = conf.Members[last].name()
            alg = self._appMgr.algorithm(name)._ialg
            if not alg:
                print 'Cannot get algorithm %s for %s' % (name, line)
            self.__algorithms[line + 'Decision'] = alg

    def run( self ):
        
        evt = self._appMgr.evtsvc()
        nEvents = self._config[ 'EvtMax' ]
        event = 1
        
        # the event loop
        while True:
            if self.wait():
                self._condition.acquire()
            
            gaudi = self._appMgr
            gaudi.algorithm("HltOutputSequence").Enable = False
            gaudi.run(1)
            
            # Check if there is still event data
            if not bool( evt[ '/Event' ] ):
                self.done()
                break
            odin = evt['DAQ/ODIN']

            reports = dict()
            #reports[ 'event' ] = odin.eventNumber()
            #reports[ 'run' ] = odin.runNumber()
            # Grab the HltDecReports and put the decisions in a dict by line name
            for loc in ('Hlt1/DecReports', 'Hlt2/DecReports'):
                decReports = evt[loc]
                if not decReports:
                    continue

                names = decReports.decisionNames()
                for name in names:
                    if name.endswith('Global'):
                        continue
                    alg = self.__algorithms.get(name, None)
                    if alg:
                        reports[ name ] = alg.isExecuted()
                    else:
                        reports[ name ] = decReports.decReport(name).decision()

            self._outQueue.put( reports )
            try:
                keep = False
                stilltofire = self._inQueue.get()
                if stilltofire:
                    for k,v in reports.iteritems():
                        if v and k in stilltofire:
                            print 'Keeping event for %s' % k
                            keep = True
                #keep = True
                if keep:
                    gaudi.algorithm("HltOutputSequence").Enable = True
                    gaudi.algorithm("HltOutputSequence")._ialg.execute()
                    gaudi.algorithm("HltOutputSequence").Enable = False
            except:
                print 'cant get stilltofire'
            ##
            event += 1
            if event == nEvents:
                self.done()
                break
            elif self.wait():
                self._condition.wait()
                self._condition.release()

    def finalize( self ):
        self._appMgr.exit()

    def done( self ):
        # Max events reached, signal done to the main process
        self._outQueue.put( 'DONE' )
        if self.wait():
            self._condition.release()

    def wait( self ):
        return self._config.get('Wait', False)

def run( options, args ):

    print 'Starting run method'

    #EventSelector().PrintFreq = 1

    from Configurables import Moore
    from Configurables import CondDB
    from Configurables import L0Conf, HltConf

    # Put the options into a dictionary
    reporterConfig = dict()
    reporterConfig[ 'ThresholdSettings' ] = "Physics_pp_May2016" #options.Settings 
    reporterConfig[ 'EvtMax' ] = options.EvtMax
    reporterConfig[ 'Wait' ] = False     ## Don't wait after each event
    reporterConfig[ 'CheckOdin']= False;
    reporterConfig[ 'Split'] = '' #'Hlt1'
    reporterConfig[ 'EnableTimer'] = True
    reporterConfig[ 'EnableOutputStreaming'] = False
    reporterConfig[ 'RemoveInputHltRawBanks'] = True
    reporterConfig[ 'ForceSingleL0Configuration'] = False
    reporterConfig[ 'UseTCK']= False;
    reporterConfig[ 'Simulation']= False;
    reporterConfig[ 'OutputLevel']= 3
    reporterConfig[ 'RequireRoutingBits' ] = [0x0, 0x4, 0x0]
    
    #### get the data for the subjobs
    from PRConfig import TestFileDB
    dataset = TestFileDB.test_file_db['2015NB_25ns_L0Filt0x0050']
    files = dataset.filenames
    
    #reporterConfig[ 'DDDBtag' ] = dataset.qualifiers['DDDB']
    #reporterConfig[ 'CondDBtag' ] = dataset.qualifiers['CondDB']
    reporterConfig[ 'DDDBtag' ] = options.DDDBtag
    reporterConfig[ 'CondDBtag' ] = options.CondDBtag

    files = ['/scratch/hlt/make_tcks/turcal_164699_0000000087.raw',
             '/scratch/hlt/make_tcks/turbo_164699_0000000088.raw',
             '/scratch/hlt/make_tcks/full_164699_0000000090.raw']
    #'/localdisk/bw_division/Mika050116_L0Filtered_0x0050/2015NB_25ns_0x0050_0.mdf',
    #         '/localdisk/bw_division/Mika050116_L0Filtered_0x0050/2015NB_25ns_0x0050_1.mdf',
    #         '/localdisk/bw_division/Mika050116_L0Filtered_0x0050/2015NB_25ns_0x0050_2.mdf']
    #files = ['/localdisk/bw_division/Mika050116_L0Filtered_0x0050/2015NB_25ns_0x0050_0.mdf',
    #         '/localdisk/bw_division/Mika050116_L0Filtered_0x0050/2015NB_25ns_0x0050_1.mdf',
    #         '/localdisk/bw_division/Mika050116_L0Filtered_0x0050/2015NB_25ns_0x0050_10.mdf',
    #         '/localdisk/bw_division/Mika050116_L0Filtered_0x0050/2015NB_25ns_0x0050_11.mdf',
    #         '/localdisk/bw_division/Mika050116_L0Filtered_0x0050/2015NB_25ns_0x0050_12.mdf',
    #         '/localdisk/bw_division/Mika050116_L0Filtered_0x0050/2015NB_25ns_0x0050_13.mdf',
    #         '/localdisk/bw_division/Mika050116_L0Filtered_0x0050/2015NB_25ns_0x0050_14.mdf',
    #         '/localdisk/bw_division/Mika050116_L0Filtered_0x0050/2015NB_25ns_0x0050_2.mdf',
    #         '/localdisk/bw_division/Mika050116_L0Filtered_0x0050/2015NB_25ns_0x0050_3.mdf',
    #         '/localdisk/bw_division/Mika050116_L0Filtered_0x0050/2015NB_25ns_0x0050_4.mdf',
    #         '/localdisk/bw_division/Mika050116_L0Filtered_0x0050/2015NB_25ns_0x0050_5.mdf',
    #         '/localdisk/bw_division/Mika050116_L0Filtered_0x0050/2015NB_25ns_0x0050_6.mdf',
    #         '/localdisk/bw_division/Mika050116_L0Filtered_0x0050/2015NB_25ns_0x0050_7.mdf',
    #         '/localdisk/bw_division/Mika050116_L0Filtered_0x0050/2015NB_25ns_0x0050_8.mdf',
    #         '/localdisk/bw_division/Mika050116_L0Filtered_0x0050/2015NB_25ns_0x0050_9.mdf']
    ### make the subjobs themselves
    wrappers = dict()
    NProcesses = min(len(files),options.NProcesses)
    datasets = Splitter(files,NProcesses) ## distribute the list of files across the child processes
    for i in range(0,NProcesses):
        allConfig = reporterConfig.copy()
        allConfig['Input'] = datasets[i]
        wrappers[ 'Moore%02d'%i ] = ProcessWrapper(i, DecisionReporter, 'Moore%02d'%i, allConfig,
                                                   False) #last arg is debug
        

    # Keep track of jobs that have completed
    completed = set()
    running = set()
    todo = set()

    print 'starting child processes'
    #### start the child processes
    lines = set()
    
    # start the rest of the processes
    print 'starting %s of processes (total = %s)' %(NProcesses,len(wrappers.keys()))
    for name, wrapper in wrappers.iteritems():
        if len( running ) == NProcesses:
            break
        print name
        wrapper.start()
        running.add( name )
    todo = set( wrappers.keys() ).difference( running )
    
    if options.debug:
        print 'running %s' %running
        print 'completed %s' %completed
        print 'todo %s' %todo
    call = 0
    stilltofire = set()
    lasttime = time()
    while True:
        call += 1
        if call % 1000 == 0 and lines:
            print '-' * 100
            print 'call %s/%s' %(call,reporterConfig['EvtMax']*len(wrappers))
            print 'throughput = %s Hz' % (1000./(time() - lasttime))
            print 'stilltofire (%s/%s) %s' % (len(stilltofire), len(lines), sorted(list(stilltofire)))
            print '-'*100
            lasttime = time()
        if options.debug:
            print 'running %s' %running
            print 'completed %s' %completed
            print 'todo %s' %todo
        if len( completed ) == len( wrappers ):
            print 'all jobs completed'
            print 'completed = %s' %len(completed)
            print 'requested = %s' %len(wrappers)
            break
        ### still jobs running
        ready = None
        try:
            ( ready, [], [] ) = select.select( [ wrappers[ name ] for name in running ], [], [] )
            for wrapper in ready:
                
                data = wrapper.getData( False )
                if not lines:
                    lines = set(data.keys())
                    stilltofire = set(data.keys())
                ###!!!!!!!!!!
                ###!!!!!!!!!!
                wrapper.putData( deepcopy(stilltofire) )
                if options.debug:
                    print 'inspecting %s' %(wrapper.name())
                    print 'data:'
                    print data                    
                if data == None:
                    continue
                elif type( data ) == type( "" ):
                    if data == 'DONE':
                        completed.add( wrapper.name() )
                        wrapper.join()
                        running.difference_update( completed )
                else:
                    name = wrapper.name()
                    for line, dec in data.iteritems():
                        if dec and line in stilltofire:
                            stilltofire.remove(line)

        except select.error:
            sleep(5)
            print 'select.error'

    print '='*100
    print 'stilltofire (%s/%s) %s' % (len(stilltofire), len(lines), sorted(list(stilltofire)))
    print '='*100
    return 0
    
if __name__ == "__main__":

    # Setup the option parser
    usage = "usage: %prog [options] [TestFileDB_dataset]"
    parser = optparse.OptionParser( usage = usage )

    parser.add_option( "--DataType", action="store", dest="DataType", 
                       default="2015", help="DataType to run on.")
    
    parser.add_option( "-n", "--evtmax", action = "store", type = 'int',
                       dest = "EvtMax", default = 1.e4, help = "Number of events to run" )
    
    parser.add_option( "--Settings", action = "store", dest = "Settings",
                       default = "Physics_pp_May2016", help = "Threshold settings" )
    
    parser.add_option( "--nprocesses", action = "store", type = 'int',
                       dest = "NProcesses", default = 12,
                       help = "Number of simultaneous processes to run." )
    
    parser.add_option( "--dddbtag", action="store", dest="DDDBtag",
                       default="dddb-20150724", help="DDDBTag to use" )
    
    parser.add_option( "--conddbtag", action = "store", dest = "CondDBtag",
                       default = 'cond-20150828', help = "CondDBtag to use" )
    
    parser.add_option( "-v", "--verbose", action = "store_true", dest = "Verbose",
                       default = False, help = "Verbose output" )

    parser.add_option( "-d","--debug", action="store_true", dest="debug",
                       default=False, help="debug" )

    # Parse the command line arguments
    (options, args) = parser.parse_args()

    ## multiprocessing.log_to_stderr()
    ## logger = multiprocessing.get_logger()
    ## logger.setLevel(logging.INFO)

    #if not len( args ):
    #    print "No settings specified"
    #    exit( 1 )

    sys.exit( run( options, args ) )
