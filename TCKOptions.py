"""Configuration for creation and testing of split TCKs.

E.g. for the first step, do like:
./run gaudirun.py CreateTCK1.py TCKOptions.py 2>&1 | tee CreateTCK1.log
The script CreateTCK1.py contains the absolute minimum configuration for that step
"""

from Configurables import Moore, CondDB
from Moore import Funcs
outputTrans = {".*Factory.*" : {"UsePython" : {"^.*$": "False"}}}
Funcs._mergeTransform(outputTrans)

# Options for running over new data at the pit
Moore().Split = "Hlt2"
Moore().UseTCK = True
Moore().inputFiles = ["/net/hltf1111/localdisk1/hlt1/Run_0194681_20170710-014652.hltf1111.mdf"]
Moore().DataType = "2017"
import sys
rd = '/group/online/hlt/conditions/RunChangeHandler'
sys.path.append(rd)
CondDB().UseDBSnapshot = True
CondDB().EnableRunChangeHandler = True
CondDB().EnableRunStampCheck = False
CondDB().IgnoreHeartBeat = True
CondDB().Tags['ONLINE'] = 'onl-20170512'
from Configurables import MagneticFieldSvc
MagneticFieldSvc().UseSetCurrent = True
import All
CondDB().RunChangeHandlerConditions = All.ConditionMap


# !!! Check that these settings are what we want for these TCKs !!! #
settings = {
    # Modify these as approppriate for the new TCKs:
    "TCKData": "TCKData",
    # "TCKData": "TCKData",
    "L0TCK": "0x17a7",
    "Hlt1TCK": "0x115417a7",
    "Hlt2TCK": "0x215517a7",
    "Hlt1Label": "Hlt1, Physics pp July 2017, 2308b, with SumEt Prev. and Herschel, LBHLT-349-2",
    "Hlt2Label": "Hlt2, Physics pp July 2017, 2308b, with SumEt Prev. and Herschel, LBHLT-349-2",
    "HltType": "Physics_pp_July2017",
    # Check if these need to be updated:
    "ThresholdSettings": "Physics_pp_2017",
    "DDDBtag": "dddb-20150724",
    "CondDBtag": "cond-20170510",
}


import os
from Gaudi.Configuration import allConfigurables

if 'Moore' in allConfigurables:  # For the Create/Test TCK steps
    from Moore.Configuration import Moore, ApplicationMgr

    # Options that are defined in the CreateTCK/TestTCK script
    split = Moore().getProp('Split')
    useTCK = Moore().getProp('UseTCK')

    # Standard Moore options that should be the same for all TCK creation and testing steps
    Moore().EvtMax = -1
    Moore().CheckOdin = False

    # For the Hlt1 steps, we need to remove the existing Hlt raw banks
    if split == "Hlt1":
        Moore().RemoveInputHltRawBanks = True

    # Specify the location for our local config.cdb file
    TCKData_DEST = os.path.expandvars(settings['TCKData'])
    #Moore().TCKData = TCKData_DEST
    if not os.path.exists(TCKData_DEST):
        os.makedirs(TCKData_DEST)

    Moore().DDDBtag = settings['DDDBtag']
    Moore().CondDBtag = settings['CondDBtag']

    # TODO remove this when we put the TCK creation sample in the TestFileDB
    from Configurables import EventClockSvc
    #EventClockSvc(InitialTime=1465516800000000000)  # June 10, 2016

    if useTCK:
        # for the TestTCK steps
        Moore().InitialTCK = settings[split + "TCK"]

        # Disable the functor cache when testing the TCK to get rid of:
        #   You are probably missing the definition of void boost::ignore_unused<int>(int const&)
        # TODO Remove this once the error is resolved
        #ApplicationMgr().Environment['LOKI_DISABLE_CACHE'] = '1'

        # This will require that the functor cache has been built properly.
        # TODO Enable this when we testing the cache becomes part of
        #      the TCK creation procedure
        # from Moore import Funcs
        # outputTrans = {".*Factory.*": {"UsePython": {"^.*$": "False"}}}
        # Funcs._mergeTransform(outputTrans)
    else:
        # for the CreateTCK steps
        Moore().generateConfig = True
        Moore().ThresholdSettings = settings["ThresholdSettings"]
        Moore().HltType = settings["HltType"]  # override the HltType in the settings
        Moore().configLabel = settings.get(split + "Label", "")
        Moore().EnableOutputStreaming = True

        # overide the L0TCK in the threshold settings
        from Configurables import HltConf
        HltConf().setProp("L0TCK", settings["L0TCK"])

        # Disable the functor cache when creating the TCK
        ApplicationMgr().Environment['LOKI_DISABLE_CACHE'] = '1'
        # Overrule property to use cache at the end of the TCK creation
        from Configurables import HltGenConfig
        factories = ['ToolSvc.Hlt1HybridFactory', 'ToolSvc.TrackFunctorFactory',
                     'ToolSvc.Hlt1CoreFactory', 'ToolSvc.CoreFactory',
                     'ToolSvc.Hlt1HltFactory', 'ToolSvc.Hlt1Factory',
                     'ToolSvc.TrackFactory', 'ToolSvc.Hlt2TrackFunctorFactory',
                     'ToolSvc.Hlt2HltFactory', 'ToolSvc.Hlt2CoreFactory',
                     'ToolSvc.Hlt2HybridFactory', 'ToolSvc.HltFactory']
        HltGenConfig().Overrule = {f: ['UseCache:True'] for f in factories}

elif 'L0App' in allConfigurables:  # For the RedoL0 step
    from L0App.Configuration import L0App
    L0App().TCK = settings['L0TCK']
    L0App().DDDBtag = settings['DDDBtag']
    L0App().CondDBtag = settings['CondDBtag']
