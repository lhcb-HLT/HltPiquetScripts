#############################################################
# Code to get a list with the runs affected by sharp and/or
# wide rate drops in L0 and HLT1.
# 
# @author: Miguel Ramos Pernas
# @email:  miguel.ramos.pernas@cern.ch
# @date:   07/11/2016
#############################################################

import os
import ROOT as rt
import numpy as np
import scipy.stats as st
from math import sqrt
from hlt1rate_utils import *
from hlt1rate_print import *

# ---------------------
# Configuration options

# Number of bins not considered (begining/end)
DUMPBINS_BEG = 60
DUMPBINS_END = 10
# Minimum number of entries in the histogram to be processed
MINENTRIES   = 1e4
# Minimum rate to be processed (for both L0 and HLT1)
MINRATE      = {'l0': 7e5, 'hlt1': 7e4}
# Minimun number of points at low rate to consider the run as bad
NPOINTS      = 4
# Fraction with respect to the maximum value of the rate that must satisfy
# each of the low-rate points to consider the run as bad
MINPOINTFRAC = 0.4
# Maximum value for the chi2 calculated with respect to the mean
CLEANCHI2 = 1e5
# Output file to save the histograms
OFILE = rt.TFile( 'hlt1Rates.root', 'RECREATE' )
# Output folder to save the pdf files
FOLDER = './plots'
# Date to do the search. At least the year must be provided.
YEAR  = '2016'
MONTH = False
DAY   = False
# ---------------------------------------------------

# Create output folder if it does not exist
if not os.path.isdir(FOLDER):
    os.system('mkdir ' + FOLDER)
    print '--- Created output folder < %s >' %FOLDER
else:
    dec = raw_input('INFO: Shall I remove all contents inside %s? (y/n): ' %FOLDER)
    while dec not in ('y', 'n'):
        dec = raw_input('WARNING: Please answer (y/n)')
    if dec == 'y' and os.listdir(FOLDER):
        print '--- Removing all files in %s' %FOLDER
        os.system('rm %s/*'%FOLDER)

# Return the run number of a file
def getRunNumber( name ):
    name = name.split('run')[-1]
    return name.split('.root')[0]

# Removes the bins with a content of zero. It also removes the events
# outside of the desired timescale (>0).
def removeZeros( h ):
    nozeros = np.array([h.GetBinContent( i ) != 0 for i in xrange(h.GetNbinsX())])
    postime = np.array([h.GetBinCenter( i ) > 0 for i in xrange(h.GetNbinsX())])
    mask    = nozeros*postime
    lst     = np.array([ h.GetBinContent(i)
                         for i in xrange(h.GetNbinsX()) if mask[i] ])
    return lst[DUMPBINS_BEG:-DUMPBINS_END]

# This is the function to be called to decide if the run is affected or not
def decMethod( trig, h, run ):
    blst = removeZeros( h )
    if len(set(blst)) > 2 and h.GetSumOfWeights() > MINENTRIES and max(blst) > MINRATE[trig]:
        
        '''
        It is marked as affected if some point is below a certain
        threshold for too many time. The cleaning factor is also
        taken into account to prevent from storing weird shapes.
        '''
        dec = (len(blst[blst < max(blst)*MINPOINTFRAC]) > NPOINTS)
        
        maxblst  = blst[blst > MINPOINTFRAC]
        ln       = len(maxblst)
        mean     = np.mean(maxblst)*np.ones(ln)
        cleaning = (st.chisquare(maxblst, mean)[0]/(ln - 1) < CLEANCHI2)

        if dec and cleaning:
            return 1
        else:
            return 0
    else:
        return -1

# Make the histograms    
def makeTriggerHists( h, run ):
    blst   = removeZeros( h )
    dist   = blst - np.mean(blst)
    stddev = np.std(blst)
    vmin, vmax = min(dist), max(dist)
    add  = (vmax - vmin)/50.
    h.SetName( h.GetTitle() + '_' + run.name() )
    h.SetTitle( h.GetName() )
    name = 'nStd_' + h.GetName()
    hstd = rt.TH1D( name, name, 100, vmin - add, vmax + add )
    for val in dist:
        hstd.Fill( val/stddev )
    return h, hstd

# Process a trigger (hlt1/l0) for a run number
def processTrigger( ifile, ofile, run, l0_fname, hlt1_fname ):

    rt.gROOT.SetBatch(1)
    affed = False

    hists = {}
    decs  = []
    for name, fname, hist in ( ('l0', l0_fname, 'HltRate_L0_Physics'),
                              ('hlt1', hlt1_fname, 'HltRate_HLT1_Global')):

        fold = ifile.Get(fname)

        if fold:
            hists[name] = fold.Get(hist)
        else:
            hists[name] = 0
            print 'WARNING: No input information in:', fname
    
        if not hists[name]:
            print 'WARNING: No input histogram:', hist
            return False
    
        decs.append( decMethod( name, hists[name], run ) )

    if sum(decs) > 0:
        affed  = True
        foldnm  = 'Rates_' + run.num + '_' + run.time
        ofolder = ofile.Get( foldnm )
        if not ofolder:
            ofolder = ofile.mkdir( foldnm )
        ofolder.cd()
        for nm, h in hists.iteritems():
            for h in makeTriggerHists( h, run ):
                h.Write()
           
    rt.gROOT.SetBatch(0)

    return affed

# -----------------------------------------------------------------------------

if __name__ == '__main__':

    # Locate and store the paths for the files associated with each run number
    runNmap = buildRunNumberMap(YEAR, MONTH, DAY)

    # Iterates in the given path processing all the files within it
    cleanlist  = []
    affedlist = []

    for rn in runNmap:
        fpath = rn.moorepath()
        print '--- Accessing input file:', fpath
        ifile = rt.TFile( fpath )
    
        OFILE.cd()

        affed = processTrigger( ifile, OFILE, rn,
                                 'Hlt1OnlineL0RateMonitor',
                                 'Hlt1OnlineHLT1RateMonitor')

        if affed:
            affedlist.append( rn.num )
        else:
            cleanlist.append( rn.num )
        
        ifile.Close()

    # Get the statistics about the affected files (distribution of affected runs per month)
    affedmap = runList(rn for rn in runNmap if rn.num in affedlist)
    path = '/'.join([el for el in [YEAR, MONTH, DAY] if el])
    h = histFromDict( 'TimeDist', affedmap.stats(path) )
    h.Write()
    
    # Print the output plots
    plotFromFile(OFILE)

    # Display the list of affected runs
    naffed = len(affedlist)
    nclean = len(cleanlist)
    ntotal = naffed + nclean
    print '--- Affected runs:', affedlist
    print '--- Number of affected runs:', naffed
    print '--- Total number of runs:', ntotal
    print '--- Percentage:', naffed*100./ntotal, '%'

    # Close the output file
    OFILE.Close()
