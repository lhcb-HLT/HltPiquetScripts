#############################################################
# Code to configure ROOT and to print the plots generated
# with hlt1rate.py
# 
# @author: Miguel Ramos Pernas
# @email:  miguel.ramos.pernas@cern.ch
# @date:   07/11/2016
#############################################################

import ROOT as rt
from hlt1rate_utils import *

# Style setup
def setupRoot():
    rt.gROOT.SetBatch()
    rt.gStyle.SetOptStat(0)
    rt.gStyle.SetLineWidth( 2 )
    rt.gStyle.SetTitleSize(0.04)
    rt.gStyle.SetLegendTextSize(0.04)

# Plot
def plotFromFile( ifile ):
    setupRoot()
    for kw in ifile.GetListOfKeys():
        name = kw.GetName()
        obj  = ifile.Get(name)
        if isinstance(obj, rt.TH1):
            c = rt.TCanvas(name, name)
            obj.GetYaxis().SetTitle('Number of occurrences')
            obj.GetXaxis().SetTitle('Month')
            obj.Draw('H')
        else:
            c = rt.TCanvas(name, name)
            hlst = []
            legend = rt.TLegend( 0.65, 0.75, 0.9, 0.9 )
            col = 2
            for el in obj.GetListOfKeys():
                if 'nStd' not in el.GetName():
                    h = obj.Get(el.GetName())
                    h.SetTitle(el.GetName()[-33:])
                    h.SetLineColor(col)
                    h.SetMarkerColor(col)
                    hlst.append(h)
                    legend.AddEntry( h, h.GetName()[8:-34], 'L' )
                    col += 2
            hlst.sort(key=lambda h: -h.GetMaximum())
            for h in hlst:
                h.GetYaxis().SetRangeUser(1, 5e6)
                h.GetYaxis().SetTitle('Rate (Hz)')
                h.GetXaxis().SetTitle('Time (s)')
                h.Draw('SAMEH')
            legend.Draw('SAME')
            c.SetLogy()
        c.Print('./plots/' + c.GetName() + '.pdf')

if __name__ == '__main__':
    ifile = rt.TFile('hltRate.root')
    plotFromFile(ifile)
    ifile.Close()
