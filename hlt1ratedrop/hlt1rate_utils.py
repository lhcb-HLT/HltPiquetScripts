####################################################
# Some classes and functions to use in hlt1rate.py
# 
# @author: Miguel Ramos Pernas
# @email:  miguel.ramos.pernas@cern.ch
# @date:   07/11/2016
####################################################

import os
import ROOT as rt
from math import sqrt

# -------------------------------------------------
# CLASSES

class runList(list):
    '''
    List of run numbers
    '''
    def __init__( self, *args ):
        '''
        The contruction is the same as for a python list,
        but the type of the arguments is checked
        '''
        super(runList, self).__init__(*args)
        if not all(isinstance(el, runNum) for el in self):
            print 'ERROR: runList object constructed with non-runNum classes'
            
    def sortbytime( self ):
        ''' Sort this list by run number '''
        self.sort(key=lambda x: int(x.time))

    def runnumbers( self ):
        ''' Returns a list with the run numbers in the list '''
        return [rn.num for rn in self]
        
    def sublist( self, path ):
        '''
        Creates another runList object whose object
        belong to the given path
        '''
        return runList( rn for rn in self if rn.isin( path ) )
    
    def stats( self, path ):
        ''' Create the statistic plots associated to the given date '''
        
        lst   = self.sublist( path )
        spath = ymdSplit( path )
        lp    = len(spath)

        if lp == 1:
            sellst = [(1 - i/10)*'0' + str(i) for i in xrange(1, 13)]
        elif lp == 2:
            sellst = [(1 - d/10)*'0' + str(d) for d in xrange(1, 32)]
        elif lp == 3:
            sellst = lst.runnumbers()

        odict = {sel: 0 for sel in sellst}
        for sel in sellst:
            for rn in lst:
                if rn.isin( path + '/' + sel ):
                    odict[ sel ] += 1
        return odict

class runNum:
    '''
    Defines the run numer class, containing the
    basic information of the run
    '''
    def __init__( self, num, year, month, day, time ):
        '''
        As input, all the parameters must be given
        '''
        self.num   = num
        self.year  = year
        self.month = month
        self.day   = day
        self.time  = time
        
    def __repr__( self ):
        ''' Information displayed by < print > '''
        return self.__str__()
    
    def __str__( self ):
        ''' Conversion to string '''
        return '[' + ','.join([self.num, self.year,
                               self.month, self.day, self.time]) + ']'

    def name( self ):
        return '_'.join([self.num, self.year, self.month, self.day, self.time])
        
    def isin( self, path ):
        ''' Checks whether this run belongs to a given date '''
        lst = ymdSplit( path )
        return all(tp == lt for tp, lt in zip([self.year, self.month, self.day], lst))
    
    def date( self ):
        ''' Returns the date in a single string '''
        return '/'.join([self.year, self.month, self.day ])

    def moorepath( self ):
        '''
        Returns the path to the Moore file associated
        with this class
        '''
        path = os.path.sep.join(['/hist/Savesets', self.year,
                                 'LHCb/Moore1HistAdder', self.month, self.day])
        for f in os.listdir(path):
            if self.num in f.split('-'):
                return path + '/' + f

# -------------------------------------------------
# FUNCTIONS

# Build map of run-number -> date
def buildRunNumberMap( year, month = False, day = False ):
    print '--- Building run-number map'
    runnumlst = runList()
    path      = '/hist/Savesets/' + year + '/LHCb/Moore1HistAdder/'
    if month:
        months = [month]
    else:
        months = sorted(os.listdir(path))
    if day:
        dayspermonth = {month: [day]}
    else:
        dayspermonth = {m: sorted(os.listdir(os.path.sep.join([path, m])))
                        for m in months}
    for m, ds in dayspermonth.iteritems():
        for d in ds:
            ipath = os.path.sep.join([path, m, d])
            files = sorted(os.listdir(ipath))
            for f in files:
                splt   = f.split('-')
                runnum = splt[1]
                time   = splt[2]
                runnumlst.append( runNum( runnum, year, m, d, time ) )
    return runnumlst

# Create a labeled histogram with the occurrences for each bin
def histFromDict( name, dct ):
    l = len(dct)
    h = rt.TH1I( name, name, l, 0, l )
    h.SetLineWidth( 2 )
    h.SetLineColor( rt.kBlue )
    h.SetFillColor( rt.kBlue )
    h.SetFillStyle( 3004 )
    kws = sorted(dct.keys())
    for ib, k in enumerate(kws):
        ib += 1
        h.GetXaxis().SetBinLabel( ib, k )
        h.SetBinContent( ib, dct[k] )
    return h

# Split a path in year, month and day
def ymdSplit( path ):
    lst = path.split('/')
    lst = filter( lambda x: x != '', lst )
    if len(lst) > 3:
        print 'ERROR: Input path must contain at most year/month/day'
    return lst
