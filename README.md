## Old Hlt/HltPiquetScripts
HltPiquetScripts used to live on [svn](https://svnweb.cern.ch/trac/lhcb/browser/packages/trunk/Hlt/HltPiquetScripts)
and could be obtained locally with `getpack` as it was a standard Gaudi package.

Now all scripts and modules are on git, with a slightly changed structure.
To use them, a typical workflow is e.g.
```sh
git clone https://gitlab.cern.ch/lhcb-HLT/HltPiquetScripts.git
cd HltPiquetScripts
lb-run MooreOnline/latest python scripts/RedoL0.py
```
