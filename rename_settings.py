
import os
import shutil
def inplace_change(filename, old_string, new_string):
    # Safely read the input filename using 'with'
    with open(filename) as f:
        s = f.read()
        if old_string not in s:
            print '"{old_string}" not found in {filename}.'.format(**locals())
            return

    # Safely write the changed content, if found in the file
    with open(filename, 'w') as f:
        print 'Changing "{old_string}" to "{new_string}" in {filename}'.format(**locals())
        s = s.replace(old_string, new_string)
        f.write(s)
        
subdirs = ['CcDiHadron', 'DPS', 'Bc2JpsiX','Bottomonium','B2HH','B2Kpi0','BHad','CharmHad','DiElectron','Exotica',
           'Jets','LFV','LowMult','PID','Phi','Radiative','RareCharm','EW',
           'Topo','TrackEffDiMuon','DisplVertices','XcMuXForTau','Technical','TrackEff',
           'DiMuon', 'SingleMuon','Majorana','RareStrange','TriMuon','Strange','SLB']


root = '.'
draft_suffix = '_25ns_Draft2016'
new_suffix = '_pp_June2016'

new_fns = []
for subdir in subdirs:
    draft_bn = subdir + draft_suffix + '.py'
    draft_fn = os.path.join(root, subdir, draft_bn)
    new_bn = subdir + new_suffix + '.py'
    new_fn = os.path.join(root, subdir, new_bn)
    shutil.copyfile(draft_fn, new_fn)
    inplace_change(new_fn,draft_suffix,new_suffix)
    new_fns.append(new_fn)
os.system('git add ' + ' '.join(new_fns))


draft_main_fn = os.path.join(root, 'Physics_pp_Draft2016.py')
new_main_fn = os.path.join(root, 'Physics' + new_suffix + '.py')
shutil.copyfile(draft_main_fn, new_main_fn)
inplace_change(new_main_fn,'_pp_Draft2016',new_suffix)
os.system('git add ' + new_main_fn)
