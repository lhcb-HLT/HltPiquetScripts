def getTypeToNodeMap(pathToFile = "/group/online/ecs/FarmNodes.txt"):
    inputFile = open(pathToFile)
    nodeMap  = {nodeType: [] for nodeType in nodeTypes}
    for line in inputFile.readlines():
        line = line[:-1]
        values = line.split(" ")
        if values[2] == "INSERTED": continue
        nodeMap[values[1].lower()].append(values[0].lower())
    return nodeMap

def getNodeToTypeMap(pathToFile = "/group/online/ecs/FarmNodes.txt"):
    inputFile = open(pathToFile)
    nodeMap  = {}
    for line in inputFile.readlines():
        line = line[:-1]
        values = line.split(" ")
        if values[2] == "INSERTED": continue
        nodeMap[values[0].lower()]=values[1].lower()
    return nodeMap

