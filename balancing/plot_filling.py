from datetime import datetime
from ROOT import TFile, TCanvas, TLegend, TH1D
from ROOT import kOrange, kBlue, kGreen, kBlue, kMagenta
from ROOT import gStyle, gDirectory
gStyle.SetOptStat(0)
gStyle.SetOptTitle(0)

root_file = TFile("filling.root")
tree = root_file.Get("filling")

plots = {"slow" : ("node_type == 0 && good_disks == 2", kMagenta - 3),
         "medium" : ("node_type == 1 && good_disks == 2", kOrange + 5),
         "fast_b" : ("node_type == 3 && good_disks == 2", kBlue - 3),
         "fast_nonb" : ("node_type == 2 && good_disks == 2", kGreen - 3)}

canvas = None
histos = []
legend = TLegend(0.1, 0.7, 0.3, 0.9)
for name, (cut, color) in plots.iteritems():
    args = []
    if not canvas:
        canvas = TCanvas("canvas", "canvas", 969, 400)
    else:
        args = ["same"]
    histo = TH1D(name, name, 101, -0.05, 1.05)
    histo.SetLineColor(color)
    histo.SetLineWidth(2)
    legend.AddEntry(histo, histo.GetName(), "l")
    histos.append(histo)
    tree.Draw("filling >> {}".format(histo.GetName()), cut, *args)

histos[0].GetYaxis().SetRangeUser(0, 200)
legend.Draw()

timestamp = datetime.now().strftime('%Y%m%d_%H%M')
canvas.Print("filling_{}.pdf".format(timestamp))
