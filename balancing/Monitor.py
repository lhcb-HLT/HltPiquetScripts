import pydim, re
from threading import Lock, Condition

pydim.dic_set_dns_node('ecs03')


class Monitor(object):
    def __init__(self, services, types = None):
        self.__first = True

        if type(services) == str:
            self.services = {services : services}
            self.types = {services : types if types != None else "C"}
        else:
            self.services = services
            self.types = {k : types.get(k, "C") if types != None else "C" for k in services.iterkeys()}

        self.__dim_svcs = {}
        self.__tags = {}
        for i, (name, service) in enumerate(self.services.iteritems()):
            self.__dim_svcs[name] = pydim.dic_info_service(service, self.types[name],
                                                           self.__callback, pydim.MONITORED, 0, i, None)
            self.__tags[name] = i

        self.__values = {self.__tags[k] : (None, False) for k in self.services.iterkeys()}
        self.__conditions = {i : Condition(Lock()) for i in self.__tags.itervalues()}
        self.__comb_condition = Condition(Lock())
        self.__callback_lock = Lock()

    def __callback(self, tag, val):
        self.__callback_lock.acquire()
        condition = self.__condition(tag)
        condition.acquire()
        self.__values[tag] = (val, self.__values[tag][1] | True)
        condition.notify()
        condition.release()

        self.__comb_condition.acquire()
        if all([e[1] for e in self.__values.itervalues()]):
            self.__values = {k : (v[0], False) for k, v in self.__values.iteritems()}
            self.__comb_condition.notify()
        self.__comb_condition.release()

        self.__callback_lock.release()

    def __get(self, d, k):
        if type(k) == int:
            key = k
        else:
            key = self.__tags[k]
        return d[key]

    def __condition(self, k):
        return self.__get(self.__conditions, k)

    def __value(self, k):
        return self.__get(self.__values, k)[0]

    def get(self, name = None, block = False):
        if self.__first:
            self.__first = False
            self.get(name, block)

        if name:
            condition = self.__condition(name)
        else:
            condition = self.__comb_condition


        if name == None:
            if block or any([e[0] == None for e in self.__values.itervalues()]):
                condition.acquire()
                condition.wait()
                condition.release()
            if len(self.services) == 1:
                return self.__value(self.services.keys()[0])
            else:
                return {k : self.__value(k) for k in self.services.iterkeys()}
        elif block or self.__value(name) == None:
            condition.acquire()
            condition.wait()
            condition.release()
        return self.__value(name)
