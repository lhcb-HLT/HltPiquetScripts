import re
from Monitor import Monitor

node_re = re.compile(r"hlt(?P<rack>[a-f])(?P<row>[0-9]{2})(?P<node>[0-9]{2})")

def node_type(*args):
    if len(args) == 3:
        rack, row, node = args
    else:
        m = node_re.match(args[0])
        rack, row, node = (m.group('rack'), int(m.group('row')), int(m.group('node')))
    if rack == 'b':
        return 'faster', 3
    elif rack == 'f' and row in [1, 8, 9, 10, 11, 12]:
        return 'fast', 2
    elif rack == 'c' and row in range(3, 7) and node in range(1, 5):
        return 'medium', 1
    elif rack == 'd' and row in range(4, 7) and node in range(1, 5):
        return 'medium', 1
    elif rack == 'f' and row in range(2, 8) and node in range(21, 33):
        return 'medium', 1
    elif node < 21:
        return 'slow', 0
    else:
        return 'fast', 2


def parse_nodes_runs_files(value, host_re=node_re):
    nodes_files = {}
    for entry in value.rstrip('|\x00').split('|'):
        # entry is e.g. 'hlta0422 0 183026/89,183030/99,183032/1,'
        host, _, files = entry.split(' ')
        if host_re and not host_re.match(host):
            continue
        files = files.strip(',').split(',')
        files = dict(map(lambda e: map(int, e.split('/')), files))
        if 0 in files:  # nodes with no files have a '0/0' entry
            del files[0]
        nodes_files[host] = files
    return nodes_files


def parse_storage_status(value, host_re=node_re):
    nodes_info = {}
    for entry in value.rstrip('|\x00').split('|'):
        # entry is e.g. 'hlta0105 3375.96/3167.03/2/2'
        host, numbers = entry.split(' ')
        if host_re and not host_re.match(host):
            continue
        n = numbers.split('/')
        total, free, nd, gd = float(n[0]), float(n[1]), int(n[2]), int(n[3])
        nodes_info[host] = {
            'total': float(n[0]),
            'free': float(n[1]),
            'ndisks': int(n[2]),
            'ngooddisks': int(n[3]),
        }
    return nodes_info


_hlt1_mep_factor = Monitor('RunInfo/LHCb/MEPPackingFactor', "i").get()


def summarise_buffer_info(info):
    is_hlt1 = any(k.split('_')[-1] == 'LHCb' for k in info)
    mep_factor = _hlt1_mep_factor if is_hlt1 else 1
    # Normalize names (e.g. Events_LHCb -> Events)
    info = {k.split('_')[0]: v for k, v in info.iteritems()}
    if not ('Events' in info and 'Output' in info):
        return {}
    return {
        'Events': {
            'n': info['Events']['nseen'] * mep_factor,
            'rate': info['Events']['rateseen'] * mep_factor,
        },
        'Output': {
            'n': info['Output']['nproduced'],
            'rate': info['Output']['rateproduced'],
        }
    }


def parse_nodes_buffers(value, host_re=node_re):
    nodes_info = {}
    for entry in value.rstrip('|\x00').split('|'):
        # entry is e.g. 'hlta0110 Events_LHCb/1640798/1640798/61.1683/61.1683,undefined/0/0/0.0000/0.0000,Output_LHCb/366/366/0.0000/0.0000,Input/1640798/1640798/61.1683/61.1683,'
        # or 'hlta0110 Events_LHCb2/28924339/28924240/30.9326/30.9326,undefined/0/0/0.0000/0.0000,Output_LHCb2/6106636/6106445/6.8370/6.8370,Input/28924339/28924240/30.9326/30.9326,'
        host, buffers = entry.split(' ')
        if host_re and not host_re.match(host):
            continue
        buffer_info = {}
        for e in buffers.strip(',').split(','):
            # e is e.g. 'Events_LHCb2/28924339/28924240/30.9326/30.9326'
            e = e.split('/')
            buffer_info[e[0]] = {
                'nproduced': int(e[1]),
                'nseen': int(e[2]),
                'rateproduced': float(e[3]),
                'rateseen': float(e[4]),
            }
        info = summarise_buffer_info(buffer_info)
        if info:
            nodes_info[host] = info
    return nodes_info


PARSERS = {}
PARSERS['HLTFileEqualizer/HLT1/NodesRunsFiles'] = parse_nodes_runs_files
PARSERS['FarmStatus/StorageStatus'] = parse_storage_status
PARSERS['HLTFileEqualizer/NodesBuffersEvents'] = parse_nodes_buffers
PARSERS['HLTFileEqualizer/LHCb2/NodesBuffersEvents'] = parse_nodes_buffers


SERVICE_ALIASES = {
    'NodesRunsFiles': 'HLTFileEqualizer/HLT1/NodesRunsFiles',
    'NodesStorage': 'FarmStatus/StorageStatus',
    'NodesHlt1Rates': 'HLTFileEqualizer/NodesBuffersEvents',
    'NodesHlt2Rates': 'HLTFileEqualizer/LHCb2/NodesBuffersEvents',
}


class SmartMonitor(Monitor):
    def __init__(self, services, types=None, parsers=PARSERS, aliases=SERVICE_ALIASES):
        if isinstance(services, basestring):
            services = {services: services}
        for nickname, service in services.items():
            services[nickname] = aliases.get(service, service)
        super(SmartMonitor, self).__init__(services, types)
        self.__parsers = {k: parsers[v] for k, v in self.services.iteritems()}

    def get(self, name=None, block=False, **kwargs):
        value = super(SmartMonitor, self).get(name, block)
        if isinstance(value, dict):
            return {k: self.__parsers[k](v, **kwargs) for k, v in value.iteritems()}
        else:
            name = name or self.services.keys()[0]
            return self.__parsers[name](value, **kwargs)
