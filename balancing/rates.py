#!/usr/bin/env python
import re
from itertools import product
from SmartMonitor import SmartMonitor, node_type

hlt_svc = SmartMonitor({'HLT1': 'NodesHlt1Rates', 'HLT2': 'NodesHlt2Rates'})
nodes_rates = hlt_svc.get(block=False)

type_rates = {}
for k, rates in nodes_rates.iteritems():
    type_rates[k] = {t : dict(filter(lambda e: node_type(e[0])[0] == t, rates.iteritems())) for t in ('slow', 'medium', 'fast', 'faster')}

names = {'Output' : 'output',
         'Events' : 'input'}
from collections import defaultdict
total = {stage : defaultdict(int) for stage in type_rates.keys()}
for stage, b, t in product(sorted(type_rates.keys()), ('Events', 'Output'), ('slow', 'medium', 'fast', 'faster')):
    rates = type_rates[stage][t]
    rate = sum(e[b]['rate'] for e in rates.itervalues())
    total[stage][names[b]] += rate
    if len(rates):
        print '{} {:<7s} {:<7s} {:>7.2f} {:>10.2f}'.format(stage, names[b], t, rate / len(rates), rate)

print ''
for b, stage in product(sorted(names.values()), sorted(type_rates.keys())):
    print '{} total   {:<7s} {:>10.2f}'.format(stage, b, total[stage][b])
