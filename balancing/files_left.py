import re
import numpy as npy
from ROOT import TFile, TTree
from SmartMonitor import SmartMonitor, node_type, node_re

file_svc = SmartMonitor('NodesRunsFiles')
nodes_files = file_svc.get()

#runable_files = dict(filter(lambda entry: bool(entry[1]), map(lambda e: (e[0], {k : v for k, v in e[1].iteritems() if k < 178653}), nodes_files.items())))
n_left = sorted([(k, sum(v.itervalues())) for k, v in nodes_files.iteritems()], key = lambda e: e[1])

from ROOT import TH1D, TCanvas, gStyle, TLegend, THStack
from ROOT import kOrange, kBlue, kGreen, kBlue, kMagenta, kBlack, kCyan
gStyle.SetOptStat(0)
gStyle.SetOptTitle(0)
make_hist = lambda k : TH1D("n_left_{}".format(k), "n_left_{}".format(k), 10, 0.5, n_left[-1][1] + 0.5)
colors = {"slow" : kMagenta - 3,
          "medium" : kOrange + 5,
          "fast" : kBlue - 3,
          "faster" : kCyan - 3}
histos = {k : make_hist(k) for k in colors.iterkeys()}

for host, n in n_left:
    histos[node_type(host)[0]].Fill(n)

stack = THStack("stack", "stack")

legend = TLegend(0.1, 0.7, 0.3, 0.9)
canvas = TCanvas("canvas", "canvas", 600, 400)
first = True
for l, h in histos.iteritems():
    h.GetXaxis().SetTitle("files left")
    h.GetYaxis().SetTitle("# nodes")
    h.SetFillColor(colors[l])
    h.SetLineColor(kBlack)
    h.SetLineWidth(2)
    stack.Add(h)
    legend.AddEntry(h, l, "f")

stack.Draw()
legend.Draw()

canvas.Print("files_left.pdf")
