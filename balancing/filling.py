import numpy as npy
from ROOT import TFile, TTree
from SmartMonitor import SmartMonitor, node_type, node_re
from readNodeList import getNodeToTypeMap
filling_svc = SmartMonitor('NodesStorage')
nodes_info = filling_svc.get()
print nodes_info
nodeTypeMap = getNodeToTypeMap()
root_file = TFile("filling.root", "recreate")
tree = TTree("filling", "farm node filling info")

def make_branch(name, t):
    rt = {int : 'I', float : 'D'}
    c = npy.zeros(1, dtype = t)
    b = tree.Branch(name, c, '{}/{}'.format(name, rt[t]))
    return c, b

def mapping(nodeType):
    types = ["slow","medium","fast","faster"]
    if isinstance(nodeType, basestring):
        try:
            return types.index(nodeType.lower())
        except AttributeError:
            print nodeType, " not defined"
    elif isinstance(nodeType, int):
        try:
            return types[nodeType]
        except IndexError:
            print nodeType, " too large"
    else:
        assert "Wrong input"

rack, rack_branch = make_branch('rack', int)
row, row_branch = make_branch('row', int)
node, node_branch = make_branch('node', int)
nt, nt_branch = make_branch('node_type', int)
filling, filling_branch = make_branch('filling', float)
gd, gd_branch = make_branch('good_disks', int)

for host, info in nodes_info.iteritems():
    rd = {k : i + 1 for i, k in enumerate('abcdef')}
    r, row[0], node[0] = node_re.match(host).groups()
    rack[0] = rd[r]
    filling[0] = (1 - info['free']/info['total']) if info['ngooddisks'] else 0.
    gd[0] = info['ngooddisks']
    print host
    nt[0] = mapping(nodeTypeMap[host])
    tree.Fill()

root_file.WriteTObject(tree, tree.GetName())
