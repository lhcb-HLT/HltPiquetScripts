import subprocess

def make_env(path, all_variables=True):
    # import EnvConfig
    # s = EnvConfig.Script(['--xml', path])
    # s._makeEnv()
    # env = s.env
    cmd = ['xenv', '--py', '--xml', path]
    if all_variables:
        cmd.append('-A')
    stdout = subprocess.check_output(cmd)
    env = eval(stdout, {}, {})
    return env
