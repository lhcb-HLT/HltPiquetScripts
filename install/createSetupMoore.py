#!/usr/bin/env python
import os, sys, EnvConfig
import utils

blacklist = [
"_",
"DIM_DNS_NODE",
"GNOME_DESKTOP_SESSION_ID",
"MAIL",
"USER",
"OS",
"HOST",
"HOSTNAME",
"HOMEDIR",
"HOME",
"HISTCONTROL",
"HISTIGNORE",
"SCRATCH",
"EDITOR",
"PWD",
"GAUDICVS",
"LHCBCVS",
"MYGROUPDIR",
"SRVCLASS",
"CVS_RSH",
"G_BROKEN_FILENAMES",
"PYTHON_BINOFFSET",
"VERBOSE",
"LANG",
"SSH_CONNECTION",
"SSH_ASKPASS",
"SSH_AUTH_SOCK",
"SSH_CLIENT",
"SSH_TTY",
"SELINUX_ROLE_REQUESTED",
"SELINUX_USE_CURRENT_RANGE",
"SHLVL",
"SHELL",
"QT_GRAPHICSSYSTEM",
"KDEDIRS",
"SWDIR",
"DISPLAY",
"KDE_IS_PRELINKED",
"TMP_ENV",
"TERM",
"LOGNAME",
"MOZ_NO_REMOTE",
"LS_COLORS",
"LESSOPEN",
"BASH_FUNC_module",
"BASH_FUNC_module()",
"LC_ALL",
"LC_CTYPE",
]

soft = '/cvmfs/lhcb.cern.ch'
lcg_soft = soft+'/lib/lcg'
lhcb_soft = soft+'/lib/lhcb'

def do_replace(s,rep):
  v = s
  idx=v.find(rep[0])
  while(idx >= 0):
    v = v.replace(rep[0],rep[1])
    idx=v.find(rep[0])
  return v

def make_env(xml):
  output = sys.stdout
  env = utils.make_env(xml)
  #print 'echo "[DEBUG] ++ Prepare environment for %s";'%(xml,)
  print >>output, 'if test -d /sw; then export SW_DIR=/sw;'
  print >>output, 'else export SW_DIR='+soft+'; fi;';
  print >>output, 'export SW_DIR='+soft+';   ### Hard-coded value!'
  print >>output, 'export SW_LCG=${SW_DIR}/lib/lcg;'
  print >>output, 'export SW_LHCB=${SW_DIR}/lib/lhcb;'
  for k,v in sorted(env.iteritems()):
    if k not in blacklist:
      #print >>output, 'echo "++ export %s=%s";'%(k,v)
      v = do_replace(v,(lhcb_soft,'${SW_LHCB}'))
      v = do_replace(v,(lcg_soft,'${SW_LCG}'))
      v = do_replace(v,(soft,'${SW_DIR}'))
      # v = do_replace(v,('MooreOnlinePit_','MooreOnline_'))
      print >>output, 'export %s="%s";'%(k,v)

cwd  = os.getcwd()
proj = sys.argv[1]     #os.path.basename(cwd)
version = sys.argv[2]
if len(sys.argv) == 4:
    base_dir = sys.argv[3]
else:
    base_dir = '/group/hlt/sattelite'
setup = '{3}/{0}_{1}/InstallArea/{2}/{0}.xenv'.format(proj, version, os.environ['CMTCONFIG'], base_dir)
make_env(setup)
