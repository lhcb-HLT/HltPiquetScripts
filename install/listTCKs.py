#!/usr/bin/env python
import os, sys, re
import shutil
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("version")
parser.add_argument("--cdb")

arguments = parser.parse_args()

m = re.match(r'^(v[0-9]+r[0-9]+)(p[0-9]+)?$', arguments.version)
if not m:
    parser.error('version does not match vNrM[pL] format')
moore_version = m.group(1)

from TCKUtils.utils import *
cas = ConfigAccessSvc(**({'File': arguments.cdb} if arguments.cdb else {}))
configurations = getConfigurations(cas = cas)
tcks = {}
all_tcks = []
for conf in configurations.itervalues():
    if conf.info['release'] == 'MOORE_' + moore_version:
        for tck in conf.info['TCK']:
            all_tcks += [tck]

for tck in sorted(all_tcks):
    hlt_tck = tck >> 16
    if hlt_tck not in tcks:
        tcks[hlt_tck] = tck

out = ''
for tck in tcks.itervalues():
    if out: out += ' '
    out += '0x{:08x}'.format(tck)
print out
