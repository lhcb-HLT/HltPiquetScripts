import os
import logging
log = logging.getLogger(__name__)
from LbConfiguration.SP2 import path, Error
from LbConfiguration.SP2.version import DEFAULT_VERSION, versionKey as _vkey
versionKey = lambda x: _vkey(x[0])

def listVersions(name, platform):
    '''
    Find all instances of a Gaudi-based project in the directories specified in
    the 'path' variable and return the list of versions found.

    @param name: name of the project (case sensitive for local projects)
    @param platform: binary platform id

    @return generator of pairs (version, fullpath)
    '''
    # FIXME: when we drop Python 2.4, this should become
    # 'from .version import isValidVersion'
    from LbConfiguration.SP2.version import isValidVersion
    log.debug('listVersions(name=%r, platform=%r)', name, platform)

    name_u = name.upper()
    prefix = name + '_'
    prefix_u = name_u + '_'
    prefixlen = len(prefix)

    bindir = os.path.join('InstallArea', platform)

    found_versions = set()
    for p in [dirname for dirname in path if os.path.isdir(dirname)]:
        files = set(os.listdir(p))
        # the plain project name is taken into account as 'default' version
        if DEFAULT_VERSION not in found_versions and name in files:
            fullpath = os.path.join(p, name)
            if os.path.isdir(os.path.join(fullpath, bindir)):
                found_versions.add(DEFAULT_VERSION)
                print (DEFAULT_VERSION, fullpath)

        # versions like 'Project_vXrY'
        for entry in sorted([(filename[prefixlen:], os.path.join(p, filename))
                             for filename in files
                             if filename.startswith(prefix) and
                                 isValidVersion(name, filename[prefixlen:])],
                            reverse=True, key=versionKey):
            version, fullpath = entry
            if (version not in found_versions and
                os.path.isdir(os.path.join(fullpath, bindir))):
                print entry

        # versions like PROJECT/PROJECT_vXrY
        project_dir = os.path.join(p, name_u)
        if os.path.isdir(project_dir):
            for entry in sorted([(filename[prefixlen:],
                                  os.path.join(project_dir, filename))
                                 for filename in os.listdir(project_dir)
                                 if filename.startswith(prefix_u) and
                                     isValidVersion(name,
                                                    filename[prefixlen:])],
                                reverse=True, key=versionKey):
                version, fullpath = entry
                print 'before: ', version, fullpath
                if (version not in found_versions and
                    os.path.isdir(os.path.join(fullpath, bindir))):
                    found_versions.add(version)
                    print 'after: ', entry

platform = os.environ['CMTOPT']
print 'platform = ', platform
listVersions('MooreOnline', platform)
