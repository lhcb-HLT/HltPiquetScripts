#!/usr/bin/env python
import os, sys, re
import shutil
import argparse
import subprocess
import utils

parser = argparse.ArgumentParser(prog = 'installMoore.py')

parser.add_argument("version", nargs = 1, metavar = 'version', type = str)
parser.add_argument("--debug", action = "store_true",
                    dest = "debug", default = False,
                    help = "Debug install" )
parser.add_argument("--install-dir", action = "store",
                    dest = "base_dir", default = '/group/hlt/sattelite',
                    help = "Where to install" )
parser.add_argument("--no-link", action = "store_false",
                    dest = "link", default = True,
                    help = "Create symlink" )
parser.add_argument("--manifest", action = "store_true",
                    dest = "manifest", default = False,
                    help = "Force update of TCK/HltTCK and (re)generation of manifest")
parser.add_argument("--build", action = "store_true",
                    dest = "build", default = False,
                    help = "Force (re)build." )
parser.add_argument("--scripts", action = "store_true",
                    dest = "startup_scripts", default = False,
                    help = "Force (re)generation of startup scripts" )
parser.add_argument("--dev", action = "store_true",
                    dest = "dev", default = False,
                    help = "Install a dev version for FEST" )
parser.add_argument("--branch", action = "store",
                    dest = "branch", default = 'master',
                    help = "which branch to get packages from" )
parser.add_argument("--cache", action = "store_true", default = False,
                    dest = "cache", help = "force rebuild of cache" )
parser.add_argument("--no-install", action = "store_true", default = False,
                    dest = "no_install", help = "Run only make, not make install" )
parser.add_argument("--local-tck", action = "store_true",
                    help = "Install TCK/HltTCK locally. Hard on the /group nfs!" )

arguments = parser.parse_args()

from LbConfiguration.SP2.lookup import listVersions
platform = os.environ['CMTDEB' if arguments.debug else 'CMTOPT']
versions = list(listVersions("MooreOnline", platform))
vers = [v[0] for v in versions]

version = arguments.version[0]
if arguments.version[0] not in vers:
    print 'Unknown version, must be one of %s' % vers
    sys.exit(1)

# Base installation directory
base_dir = arguments.base_dir
if not os.path.exists(base_dir):
    os.makedirs(base_dir)
os.chdir(base_dir)

online_user_dir = '/group/online/dataflow/cmtuser'

# Sattelite area
install_dir = os.path.join(base_dir, 'MooreOnlinePit_%s' % version)
if arguments.dev:
    install_dir += 'dev'

if not os.path.exists(install_dir):
    # Setup sattelite directory
    print 'Calling lb-dev to setup sattelite area'
    name = 'MooreOnlinePit_%s' % version
    if arguments.dev:
        name += 'dev'
    cmd = ['lb-dev', '--name=%s' % name, '--dev-dir', '/group/online/dataflow/SwData',  '--dev-dir', online_user_dir, 'MooreOnline/' + version]
    subprocess.check_call(cmd)
    # First time implies:
    arguments.manifest = True
else:
    print 'Sattelite area already present, not recreating'

# Find right online version
release_dir = dict(versions)[version]
re_online = re.compile(r"(?<!Moore)Online(?P<dev>Dev)? (?P<version>v[\d]+r[\d]+(?:p[\d]+)?)")
online_version = None
with open(os.path.join(release_dir, 'CMakeLists.txt'), 'r') as cmake_lists:
    for line in cmake_lists:
        m = re_online.search(line.strip('\n'))
        if m:
            online_version = m.group('version')

print 'Online version is: ' + online_version

lines = []
replaced = False
with open(os.path.join(install_dir, 'CMakeLists.txt'), 'r') as cmake_lists:
    for line in cmake_lists:
        if 'MooreOnlinePit_' + version in line:
            line = line.replace('MooreOnlinePit_' + version + ' HEAD', 'MooreOnlinePit ' + version)
            replaced |= True
        elif ('USE MooreOnline' in line) and ('OnlineDev' not in line):
            line = line.replace('MooreOnline ' + version, 'OnlineDev %s MooreOnline %s' % (online_version, version))
            replaced |= True
        lines.append(line)

if replaced:
    print 'Fixing up CMakeLists.txt'
    with open(os.path.join(install_dir, 'CMakeLists.txt'), 'w') as cmake_lists:
        for line in lines:
            cmake_lists.write(line)

def getgit(package, branchTag, project, force = False):
    if not os.path.exists(os.path.join(install_dir, package)) or force:
        print 'get from git', package, branchTag
        p = subprocess.Popen(["git", 'remote', 'show'], stdout = subprocess.PIPE, stderr = subprocess.PIPE)
        o, e = p.communicate()
        remotes = [l.strip() for l in o.split()]
        if project not in remotes:
            subprocess.check_call(['git', 'lb-use', project])
        else :
            subprocess.check_call(['git', 'fetch', project])
        subprocess.check_call(['git', 'lb-checkout', '{}/{}'.format(project, branchTag), package])


def getpack(package, vers = '<default>', force = False):
    if not os.path.exists(os.path.join(install_dir, package)) or force:
        print 'getpack', package, vers
        subprocess.check_call(["getpack", '-p', 'anonymous', package, vers])
    else:
        print "Package %s already present, skipping." % package


def make(target):
    env = os.environ.copy()
    env['OnlineDev_DIR'] = os.path.join(online_user_dir, 'OnlineDev_%s' % online_version, 'InstallArea', platform)
    cmd = ["make", "-j8"]
    if target:
        cmd.append(target)
    subprocess.check_call(cmd, env = env)


# Go into sattelite directory
os.chdir(install_dir)
getgit('MooreScripts', arguments.branch, 'MooreOnline')

# Update the known TCKs
if arguments.manifest:
    if arguments.local_tck:
        getpack('TCK/HltTCK', 'head', force = arguments.manifest)

        # Make version symlink for TCK
        if not os.path.exists("TCK/HltTCK/v3r9999"):
            print "Creating version symlink for local TCK package."
            os.chdir("TCK/HltTCK")
            subprocess.check_call(["ln", '-s', '.', 'v3r9999'])
            os.chdir("../..")
    make('configure')

# Update CMakeLists of Hlt/HltCache to build the TCKs we have
def prepare_cache():
    print 'Getting list of TCKs for this release'
    # ./run requires at least make configure
    cmd = ['./run', 'python', '/group/hlt/sattelite/listTCKs.py', version]
    p = subprocess.Popen(cmd, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
    o, e = p.communicate()
    tcks = [l.strip() for l in o.split('\n')][-2].split()
    if not tcks:
        print 'No TCKs available for this Moore version yet, not building cache'
        return
    else:
        print 'Generating cache for TCK configurations (L0TCK doesn\'t matter):'
        print tcks

    print 'Getting Hlt/HltCache and updating its CMakeLists.txt'
    getgit('Hlt/HltCache', version, 'Moore')
    tcks_cmake = os.path.join(install_dir, 'Hlt', 'HltCache', 'tcks.cmake')
    with open(tcks_cmake, 'w') as f:
        f.write('set(tcks {})\n'.format(';'.join(tcks)))

# Prepare building of functor cache
if not os.path.exists(os.path.join(install_dir, 'InstallArea', platform)) or arguments.cache or arguments.manifest:
    prepare_cache()

# Make install
os.chdir(install_dir)
if not os.path.exists(os.path.join(install_dir, 'InstallArea', platform)) or arguments.build or arguments.cache or arguments.manifest:
    print 'Building and installing Moore'
    make('install' if not arguments.no_install else None)

# Make environment setup file
setup_file = os.path.join(install_dir, 'InstallArea', platform, 'setupMoore.sh')
if not os.path.exists(setup_file):
    p = subprocess.Popen(['/group/hlt/sattelite/createSetupMoore.py', 'MooreOnlinePit', version, base_dir], stdout = subprocess.PIPE,
                         stderr = subprocess.PIPE)
    o, e = p.communicate()
    if p.returncode:
        print 'Failed to create environment setup script'
        print e
    if any((l for l in e.split())):
        print e
    with open(setup_file, 'w') as f:
        f.write(o)

# Write project_versions.txt
version_file = os.path.join(install_dir, 'InstallArea', 'project_versions.txt')
if not os.path.exists(version_file):
    print 'Creating project_versions.txt'
    with open(version_file, 'w') as f:
        f.write('Moore %s\n' % version)
        f.write('Online %s\n' % online_version)

# Make TCK manifest
tck_manifest = os.path.join(install_dir, 'InstallArea', 'manifest')
if not os.path.exists(tck_manifest) or arguments.manifest:
    print 'Creating TCK manifest'
    env = utils.make_env('{0}/InstallArea/{1}/MooreOnlinePit.xenv'.format(install_dir, platform))
    cmd = [
        os.path.join(env['HLTTCKROOT'], 'scripts/createTCKmanifest'),
        os.path.join(env['HLTTCKROOT'], 'config.cdb'),
        os.path.join(install_dir, 'InstallArea', 'manifests')
        ]
    r = subprocess.call(cmd, env=env)
    if r:
        print 'Failed to create TCK manifest'
        sys.exit(r)
    re_vers = re.compile('v[\d]+r[\d]+')
    manifest = os.path.join(cmd[2], 'MOORE_' + re_vers.search(version).group(0))
    if not os.path.exists(manifest):
        print "WARNING: manifest file not found, this probably means there are no TCKs for this release yet."
    else:
        shutil.copyfile(manifest, tck_manifest)

# PostInstall
if not os.path.exists(os.path.join(install_dir, "InstallArea/runMooreHlt1Online_EFF.sh")) or arguments.startup_scripts:
    print 'Creating startup scripts'
    if arguments.startup_scripts:
        for t in ('', 'Hlt1', 'Hlt2'):
            script = os.path.join(install_dir, "InstallArea/runMoore%sOnline_EFF.sh" % t)
            if os.path.exists(script):
                os.remove(script)
    env = utils.make_env('{0}/InstallArea/{1}/MooreOnlinePit.xenv'.format(install_dir, platform))
    r = subprocess.call([os.path.join(install_dir, 'MooreScripts', 'scripts', 'PostInstall.py'), 'InstallArea'],
                        env=env)
    if r:
        print 'Failed to create startup scripts'
        sys.exit(r)

# Write final symlink
link = '/group/hlt/MOORE/MooreOnline_' + version
if not os.path.exists(link) and arguments.link:
    print 'Creating symlink to publish install to ECS'
    os.symlink(install_dir, link)

# Reminder for creation of manifest file for DB tags
if not os.path.exists('/group/online/hlt/conditions/manifest/MOORE_' + version):
    print "\n\n Please remember to create the manifest file for the CondDB and DDDB tags!!!"
