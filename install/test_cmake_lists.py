import os, subprocess

install_dir = '/group/hlt/sattelite/MooreOnlinePit_v25r3'
project = 'Moore'
branchTag = 'raaij-HltCache'
package = 'Hlt/HltCache'

tcks = '0x11321605 0x21321605'
os.chdir(install_dir)
subprocess.call(['git', 'lb-checkout', '{}/{}'.format(project, branchTag), package])

cmake_lists = os.path.join(install_dir, 'Hlt', 'HltCache', 'CMakeLists.txt')
with open(cmake_lists) as lists:
    lines = []
    for line in lists:
        if line.startswith('foreach(tck'):
            lines.append('foreach(tck {})'.format(tcks))
        else:
            lines.append(line)
with open(cmake_lists, 'w') as lists:
    for line in lines:
        lists.write(line.rstrip('\n') + '\n')
